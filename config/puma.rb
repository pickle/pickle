#!/usr/bin/env puma

environment ENV['RAILS_ENV'] || 'production'

#daemonize true

pidfile "/home/deployer/apps/pickle/shared/tmp/pids/puma.pid"

stdout_redirect "/home/deployer/apps/pickle/shared/tmp/log/stdout", "/home/deployer/apps/pickle/shared/tmp/log/stderr"

threads 0, 16

bind "unix:///home/deployer/apps/pickle/shared/tmp/pickle.sock"

activate_control_app

