# Load application configuration
require 'ostruct'
require 'yaml'
 
config = YAML.load_file("#{Rails.root}/config/gateway_config.yml") || {}
gateway_config = config['common'] || {}
gateway_config.update(config[Rails.env] || {})
Gateway = OpenStruct.new(gateway_config)