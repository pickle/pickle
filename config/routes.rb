Rails.application.routes.draw do

  get 'redeems/index'
  post 'redeems' => "redeems#code_enter"
  get '/deals/redeem_code/:deal_code' => "redeems#redeem_code"
  post '/deals/validate-timings' => "deals#validate_timings"

  delete "/delete_admins/:id" => "admins#destroy"
  resources :specials
  resources :users
  resources :roles

  get "/redeems" => "redeems#index"

  devise_scope :admin do
    authenticated :admin do
      root 'home#dashboard', as: :authenticated_root
    end
    unauthenticated :admin do
      root 'devise/sessions#new', as: :unauthenticated_root
    end
  end
  get "/generate_stamp/:id" => "deals#generate_stamp"

  get "/special_branch_ids/:id" => "specials#get_branch_ids"

  get "/get_prefill_text/:id" => "businesses#get_prefill_text"

  get "/search_keywords" => "businesses#search_keywords"
  post "/add_keyword" => "businesses#add_keyword"
  post "/assign_business" => "admins#assign_business"
  get "/businesses/search_name" => "businesses#search_name"
  get "/admin_businesses/:admin_id" => "admins#get_businesses"

  post "/frequent_search/:type" => "deals#frequent_search"

  get '/get_ruby_date/:date' => "deals#get_ruby_date"

  get '/deals/week_view(/:date(/:window))' => "deals#week_view"

  #  post '/businesses/mini_search' => "businesses#mini_search"

  get '/cloudinary_upload' => "images#cloudinary_upload"

  get '/charts_by_week' => "home#charts_by_week"

  get '/invite_users', to: "admins#invite_users"
  get '/branches/search/', to: "branches#search_by_business_name"
  get '/branches/branches_deals/', to: "branches#branches_deals"
  get '/reinvite/:id', to: "admins#reinvite", as: :reinvite
  resources :admins, only: :new
  post '/send_invitation', to: "admins#send_invitation"

  resources :categories
  resources :businesses do
    collection { post :search, to: 'businesses#index' }
    collection { get :search, to: 'businesses#index' }
    collection { get :autocomplete, to: 'businesses#autocomplete' }
    resources :branches do
      collection { post :search, to: 'branches#index' }
      collection { get :search, to: 'branches#index' }
    end

    # resources :deals
  end
  resources :countries
  resources :areas
  resources :cities

  resources :deals do
    collection { post :search, to: 'deals#index' }
    collection { get :search, to: 'deals#index' }
  end

  # post '/business_approve/:id', to: "businesses#approve", as: :business_approve
  # post '/business_disapprove/:id', to: "businesses#disapprove", as: :business_disapprove
  devise_for :admins


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
