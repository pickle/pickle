# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

20.times do |n|
  name  = Faker::Commerce.department
  desc = Faker::Company.catch_phrase
  image = Faker::Avatar.image
  keywords = Faker::Company.buzzword
  Category.create!(name:  name,
               description: desc,
               image: image,
               keywords: keywords)
end

200.times do |n|
  name  = Faker::Commerce.department
  desc = Faker::Company.catch_phrase
  image = Faker::Avatar.image
  keywords = Faker::Company.buzzword
  parent_id = rand(1..20)
  Category.create!(name:  name,
               description: desc,
               parent_id: parent_id,
               image: image,
               keywords: keywords)
end


100.times do |n|
  name  = Faker::Company.name
  category_id = rand(1..20)
  desc = Faker::Company.catch_phrase
  logo = Faker::Company.logo
  keywords = Faker::Company.buzzword
  Business.create!(
          name:  name,
          category_id: category_id,
          keywords: keywords,
               description: desc,
               logo:              logo,
               )
 
end


1000.times do |n|
  name  = Faker::Company.name
  working_days = '1,0,0,0,1,1,0'
  start_time = Faker::Time.forward(100, :morning).strftime("%H:%M")
  end_time = Faker::Time.forward(100, :evening).strftime("%H:%M")  
  address = Faker::Address.street_address + Faker::Address.state
  location_id = rand(1..20)
  business_id = rand(1..100)
  landline = Faker::Company.duns_number
  mobile = Faker::Company.duns_number
  state = Faker::Address.state
  additional_info = Faker::Company.catch_phrase
  Branch.create!(name:  name,
               additional_info: additional_info,
               working_days: working_days,
               open_time: start_time,
               close_time: end_time,
               address: address,
               location_id: location_id,
               business_id: business_id,
               landline: landline,
               mobile: mobile,
               state: state,
               )
end




deal_id = 10000
5000.times do |n|
  id = deal_id
  name  = Faker::Lorem.word
  limited = rand(0..1)
  business_id = rand(0..100)
  category_id = Business.select(:category_id).where(id:business_id)
  sub_category_ids = Category.select(:id).where(parent_id:category_id)
  sub_category_id_array = sub_category_ids.to_a
  sub_category_id = sub_category_id_array.count==0 ? 100000 : sub_category_id_array[(sub_category_id_array.count * rand()).floor][:id]
  if limited==0
    original_qty=0
    all_branches=0
  else
    all_branches = rand(0..1)
    original_qty = rand(0..100)
    if(all_branches==0)
      branch_ids = Branch.select(:id).where(business_id:business_id)
      branch_id_array = branch_ids.to_a  
      branch_id = branch_id_array.count==0 ? 100000 : branch_id_array[(branch_id_array.count * rand()).floor][:id]
      BranchesDeal.create!(
          branch_id: branch_id,
          deal_id: id,
          original_qty: original_qty
        )
    end

  end
  price = Faker::Commerce.price
  deal_price = Faker::Commerce.price
  desc = Faker::Lorem.sentence
  start_date = Faker::Time.backward(100, :all).to_date
  end_date = Faker::Time.forward(100, :morning).to_date
  available_days = '1,0,1,0,1,0,1'
  start_time = Faker::Time.forward(100, :morning).strftime("%H:%M")
  end_time = Faker::Time.forward(100, :evening).strftime("%H:%M")
  highlights = Faker::Lorem.paragraph
  terms_and_conditions = Faker::Lorem.paragraph
  redeem_steps = Faker::Lorem.paragraph
  additional_info = Faker::Lorem.sentence
  business_id = rand(1..100)
  state = rand(0..1)
  app_post = rand(0..1)
  app_buyable = rand(0..1)
  cash_buyable = rand(0..1)
  bought_count = rand(0..100)
  redeem_count = rand(0..50)
  Deal.create!(
          id:deal_id,
          name:  name,
          limited: limited,
          original_qty: original_qty,
          price: price,
          deal_price: deal_price,
          description: desc,
          start_date: start_date,
          end_date: end_date,
          available_days: available_days,
          start_time: start_time,
          end_time: end_time,
          highlights: highlights,
          terms_and_conditions: terms_and_conditions,
          redeem_steps: redeem_steps,
          additional_info: additional_info,
          business_id: business_id,
          state: state,
          app_buyable: app_buyable,
          app_post: app_post,
          cash_buyable: cash_buyable,
          bought_count: bought_count,
          redeemed_count:redeem_count,
          sub_category_id: sub_category_id
               )
  deal_id += 1
end

@permission_actions = %w(create update delete list)
@permission_classes = %w(Country City Area Business Branch Category Deal User)
@permission_actions.each do |action|
  @permission_classes.each do |cl|
    Permission.create!(subject_class: cl, action: action, name: cl + action)
  end
end


Permission.create!(subject_class: "Feed", action: "list", name: "Feedlist")
Permission.create!(subject_class: "Redeem", action: "list", name: "Redeemlist")
