# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160108155442) do

  create_table "admins", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "role",                   limit: 255
    t.string   "invitation_token",       limit: 255
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer  "invitation_limit",       limit: 4
    t.integer  "invited_by_id",          limit: 4
    t.string   "invited_by_type",        limit: 255
    t.integer  "invitations_count",      limit: 4,   default: 0
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true, using: :btree
  add_index "admins", ["invitation_token"], name: "index_admins_on_invitation_token", unique: true, using: :btree
  add_index "admins", ["invitations_count"], name: "index_admins_on_invitations_count", using: :btree
  add_index "admins", ["invited_by_id"], name: "index_admins_on_invited_by_id", using: :btree
  add_index "admins", ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree

  create_table "branch_favorites", force: :cascade do |t|
    t.integer  "branch_id",  limit: 4
    t.integer  "user_id",    limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "branch_views", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "branch_id",  limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "branches", force: :cascade do |t|
    t.string   "name",            limit: 255
    t.string   "working_days",    limit: 255
    t.integer  "open_time",       limit: 4
    t.integer  "close_time",      limit: 4
    t.text     "address",         limit: 65535
    t.integer  "location_id",     limit: 4
    t.string   "landline",        limit: 255
    t.string   "mobile",          limit: 255
    t.text     "additional_info", limit: 65535
    t.boolean  "deleted",                       default: false
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "business_id",     limit: 4
    t.string   "branch_area",     limit: 255
    t.string   "highlights",      limit: 255
    t.boolean  "state"
    t.string   "longitude",       limit: 255
    t.string   "latitude",        limit: 255
    t.text     "description",     limit: 65535
  end

  create_table "branches_deals", force: :cascade do |t|
    t.integer  "deal_id",        limit: 4
    t.integer  "branch_id",      limit: 4
    t.integer  "original_qty",   limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "current_qty",    limit: 4
    t.integer  "bought_count",   limit: 4
    t.integer  "redeemed_count", limit: 4
  end

  create_table "branches_specials", force: :cascade do |t|
    t.integer  "branch_id",  limit: 4
    t.integer  "special_id", limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "business_users", force: :cascade do |t|
    t.string   "first_name",    limit: 255
    t.string   "last_name",     limit: 255
    t.string   "email",         limit: 255
    t.string   "designation",   limit: 255
    t.string   "mobile",        limit: 255
    t.string   "role",          limit: 255
    t.string   "username",      limit: 255
    t.string   "password_salt", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "businesses", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "category_id", limit: 4
    t.text     "description", limit: 65535
    t.string   "website",     limit: 255
    t.string   "facebook",    limit: 255
    t.string   "keywords",    limit: 255
    t.string   "twitter",     limit: 255
    t.string   "logo",        limit: 255
    t.boolean  "state"
    t.boolean  "deleted",                   default: false
    t.datetime "deleted_at"
  end

  create_table "categories", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.text     "description", limit: 65535
    t.string   "image",       limit: 255
    t.string   "keywords",    limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "parent_id",   limit: 4
    t.boolean  "deleted",                   default: false
    t.datetime "deleted_at"
  end

  create_table "deals", force: :cascade do |t|
    t.string   "name",                 limit: 255
    t.boolean  "limited"
    t.integer  "original_qty",         limit: 4
    t.float    "price",                limit: 24
    t.float    "deal_price",           limit: 24
    t.text     "description",          limit: 65535
    t.date     "start_date"
    t.date     "end_date"
    t.string   "available_days",       limit: 255,   default: "1,1,1,1,1,1,1"
    t.integer  "start_time",           limit: 4,     default: 0
    t.integer  "end_time",             limit: 4,     default: 86400
    t.text     "highlights",           limit: 65535
    t.text     "terms_and_conditions", limit: 65535
    t.text     "redeem_steps",         limit: 65535
    t.text     "additional_info",      limit: 65535
    t.integer  "business_id",          limit: 4
    t.boolean  "app_post"
    t.integer  "bought_count",         limit: 4
    t.integer  "redeemed_count",       limit: 4
    t.boolean  "app_buyable"
    t.boolean  "cash_buyable"
    t.boolean  "deleted",                            default: false
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "sub_category_id",      limit: 4
    t.string   "deal_type",            limit: 255
    t.boolean  "all_branches"
    t.integer  "current_qty",          limit: 4
    t.integer  "branch_id",            limit: 4
    t.boolean  "state"
  end

  add_index "deals", ["branch_id"], name: "index_deals_on_branch_id", using: :btree

  create_table "images", force: :cascade do |t|
    t.string   "original",      limit: 255
    t.string   "medium",        limit: 255
    t.string   "thumb",         limit: 255
    t.string   "resource_id",   limit: 255
    t.string   "resource_type", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "locations", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ratings", force: :cascade do |t|
    t.integer  "branch_id",  limit: 4
    t.integer  "user_id",    limit: 4
    t.float    "rating",     limit: 24
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "specials", force: :cascade do |t|
    t.string   "title",        limit: 255
    t.text     "text",         limit: 65535
    t.string   "image",        limit: 255
    t.integer  "business_id",  limit: 4
    t.boolean  "all_branches"
    t.boolean  "deleted"
    t.datetime "deleted_at"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",              limit: 255
    t.string   "mobile",             limit: 15
    t.string   "encrypted_password", limit: 255
    t.integer  "age",                limit: 4
    t.string   "gender",             limit: 10
    t.string   "name",               limit: 255
    t.integer  "otp",                limit: 4
    t.boolean  "otp_verified"
    t.boolean  "email_verified"
    t.datetime "createdAt"
    t.datetime "updatedAt"
    t.string   "authtoken",          limit: 255
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["mobile"], name: "index_users_on_mobile", unique: true, using: :btree

  add_foreign_key "deals", "branches"
end
