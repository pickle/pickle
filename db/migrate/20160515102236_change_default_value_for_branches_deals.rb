class ChangeDefaultValueForBranchesDeals < ActiveRecord::Migration
  def change
  	change_column :branches_deals, :original_qty, :integer, :default => 0
  	change_column :branches_deals, :current_qty, :integer, :default => 0
  	change_column :branches_deals, :bought_count, :integer, :default => 0
  	change_column :branches_deals, :redeemed_count, :integer, :default => 0
  end
end
