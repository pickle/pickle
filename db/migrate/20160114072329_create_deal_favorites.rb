class CreateDealFavorites < ActiveRecord::Migration
  def change
    create_table :deal_favorites do |t|
      t.integer :deal_id
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
