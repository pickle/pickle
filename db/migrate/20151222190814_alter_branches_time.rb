class AlterBranchesTime < ActiveRecord::Migration
  def change
  	change_column :branches, :open_time, :integer
  	change_column :branches, :close_time, :integer
  end
end
