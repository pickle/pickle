class AddBuyTypeToCashDealCodes < ActiveRecord::Migration
  def change
    add_column :cash_deal_codes, :buy_type, :integer, default: 1
  end
end
