class AddUserIdToPunchedCardUsers < ActiveRecord::Migration
  def change
    add_column :punched_card_users, :user_id, :integer
  end
end
