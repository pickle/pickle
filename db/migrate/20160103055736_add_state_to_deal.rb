class AddStateToDeal < ActiveRecord::Migration
  def change
    add_column :deals, :state, :boolean
    remove_column :deals, :workflow_state
  end
end
