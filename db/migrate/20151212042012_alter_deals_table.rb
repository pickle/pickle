class AlterDealsTable < ActiveRecord::Migration
  def change
  	add_column :deals, :sub_category_id, :integer
  	add_column :deals, :deal_type, :string
  	add_column :deals, :all_branches, :boolean
  	rename_column :deals, :limited_no, :original_qty
  	add_column :deals, :current_qty, :integer
  	
  end
end
