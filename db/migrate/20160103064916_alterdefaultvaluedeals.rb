class Alterdefaultvaluedeals < ActiveRecord::Migration
  def change
  	change_column :deals, :start_time, :integer, :default => 0
  	change_column :deals, :end_time, :integer, :default => 86400
	change_column_default :deals, :available_days, '1,1,1,1,1,1,1'
  end
end
