class AddLogoToBranch < ActiveRecord::Migration
  def change
  	add_column :branches, :logo, :string, default: ''
  end
end
