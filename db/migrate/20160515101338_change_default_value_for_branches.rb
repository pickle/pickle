class ChangeDefaultValueForBranches < ActiveRecord::Migration
  def change
  	change_column :branches, :open_time, :integer, :default => 0
  	change_column :branches, :close_time, :integer, :default => 0
  	change_column :branches, :state, :boolean, :default => 0
  end
end
