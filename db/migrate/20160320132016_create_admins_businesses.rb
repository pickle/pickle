class CreateAdminsBusinesses < ActiveRecord::Migration
  def change
    create_table :admins_businesses do |t|
      t.integer :admin_id
      t.integer :business_id

      t.timestamps null: false
    end
  end
end
