class RemoveCreatedByFromPunchedCardUsers < ActiveRecord::Migration
  def change
    remove_column :punched_card_users, :created_by, :integer
  end
end
