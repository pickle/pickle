class AddTermsAndConditionsAndRedeemStepsToBusinesses < ActiveRecord::Migration
  def change
    add_column :businesses, :terms_and_conditions, :text
    add_column :businesses, :redeem_steps, :text
  end
end
