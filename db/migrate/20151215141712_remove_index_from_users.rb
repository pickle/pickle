class RemoveIndexFromUsers < ActiveRecord::Migration
  def change
  	remove_index :users, [:email, :mobile]
  end
end
