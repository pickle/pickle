class AddNoOfOrdersToDeals < ActiveRecord::Migration
  def change
  	add_column :deals, :no_of_orders, :integer, :default => 0
  end
end
