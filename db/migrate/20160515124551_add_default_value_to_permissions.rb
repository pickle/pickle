class AddDefaultValueToPermissions < ActiveRecord::Migration
  def change
    change_column :permissions, :name, :string, default: ""
    change_column :permissions, :subject_class, :string, default: ""
    change_column :permissions, :action, :string, default: ""
  end
end
