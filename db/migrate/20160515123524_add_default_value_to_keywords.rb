class AddDefaultValueToKeywords < ActiveRecord::Migration
  def change
    change_column :keywords, :name, :string, default: ""
  end
end