class CreateBranchesSpecials < ActiveRecord::Migration
  def change
    create_table :branches_specials do |t|
      t.integer :branch_id
      t.integer :special_id

      t.timestamps null: false
    end
  end
end
