class RemoveIndexOnCashDealCodes < ActiveRecord::Migration
  def change
  	remove_index :cash_deal_codes, [:user_id, :branches_deal_id]
  	add_index :cash_deal_codes, [:bought_by, :branches_deal_id], :unique => true
  end
end
