class AddFieldsToDeal < ActiveRecord::Migration
  def change
  	add_column :deals, :redeem_type, :integer, default: 0
  	add_column :deals, :buy_start_date, :date
  	add_column :deals, :buy_end_date, :date
  	add_column :deals, :code_expiry_date, :date
  	add_column :deals, :redeem_description, :text
  end
end
