class AlterTableRating < ActiveRecord::Migration
  def change
  	rename_column :ratings, :business_id, :branch_id
  end
end
