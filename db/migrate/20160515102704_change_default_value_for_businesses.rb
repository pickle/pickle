class ChangeDefaultValueForBusinesses < ActiveRecord::Migration
  def change
  	change_column :businesses, :state, :boolean, :default => 0
  end
end
