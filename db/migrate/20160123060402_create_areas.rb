class CreateAreas < ActiveRecord::Migration
  def change
    create_table :areas do |t|
    	t.string :name
    	t.integer :city_id
      t.string :latitude
      t.string :longitude
      t.boolean :deleted, :default => false
      t.datetime :deleted_at
      t.timestamps null: false
    end
  end
end
