class AddHighlightsToBranches < ActiveRecord::Migration
  def change
    add_column :branches, :highlights, :string
  end
end
