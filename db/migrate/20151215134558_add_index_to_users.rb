class AddIndexToUsers < ActiveRecord::Migration
  def change
  	add_index :users, [:email, :mobile], :unique => true
  end
end
