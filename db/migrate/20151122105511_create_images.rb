class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :original
      t.string :medium
      t.string :thumb
      t.string :resource_id
      t.string :resource_type

      t.timestamps
    end
  end
end
