class AddAttachmentImages < ActiveRecord::Migration
  def change
  	create_table :attachment_images do |t|
      t.string :image
      t.integer :attachment_id
      t.boolean :deleted
      t.datetime :deleted_at
      t.timestamps null: false
    end
  end
end
