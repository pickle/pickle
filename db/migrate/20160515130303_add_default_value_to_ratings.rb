class AddDefaultValueToRatings < ActiveRecord::Migration
  def change
    change_column :ratings, :rating, :float, default: 0.0
  end
end
