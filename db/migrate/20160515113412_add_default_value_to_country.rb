class AddDefaultValueToCountry < ActiveRecord::Migration
  def change
    change_column :countries, :name, :string, default: ""
  end
end
