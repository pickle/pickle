class AddBranchRefToDeals < ActiveRecord::Migration
  def change
    add_reference :deals, :branch, index: true, foreign_key: true
  end
end
