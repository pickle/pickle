class ChangeBranchWorkingDays < ActiveRecord::Migration
  def change
  	add_column :branches, :sun, :boolean, :default => false
  	add_column :branches, :mon, :boolean, :default => false
  	add_column :branches, :tue, :boolean, :default => false
  	add_column :branches, :wed, :boolean, :default => false
  	add_column :branches, :thu, :boolean, :default => false
  	add_column :branches, :fri, :boolean, :default => false
  	add_column :branches, :sat, :boolean, :default => false
  	remove_column :branches, :working_days
  end
end
