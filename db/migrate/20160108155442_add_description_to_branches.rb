class AddDescriptionToBranches < ActiveRecord::Migration
  def change
  	add_column :branches, :description, :text
  end
end
