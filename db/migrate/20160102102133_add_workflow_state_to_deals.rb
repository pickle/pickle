class AddWorkflowStateToDeals < ActiveRecord::Migration
  def change
  	add_column :deals, :workflow_state, :string
  end
end
