class AddDeletedToImages < ActiveRecord::Migration
  def change
  	add_column :images, :deleted, :boolean
    add_column :images, :deleted_at, :datetime
  end
end
