class AddStampStatusToBusiness < ActiveRecord::Migration
  def change
  	add_column :punched_card_businesses, :stamped, :boolean, :default => 0
  end
end
