class ChangeDealAvailableDays < ActiveRecord::Migration
  def change
  	add_column :deals, :sun, :boolean, :default => false
  	add_column :deals, :mon, :boolean, :default => false
  	add_column :deals, :tue, :boolean, :default => false
  	add_column :deals, :wed, :boolean, :default => false
  	add_column :deals, :thu, :boolean, :default => false
  	add_column :deals, :fri, :boolean, :default => false
  	add_column :deals, :sat, :boolean, :default => false
  	remove_column :deals, :available_days
  end
end
