class AddNewMobileToUser < ActiveRecord::Migration
  def change
  	add_column :users, :new_mobile, :string, default: ''
  	add_column :users, :old_mobile, :string, default: ''
  	add_column :users, :new_otp, :integer, default: 0
  	add_column :users, :new_otp_verified, :boolean, default: 0

  end
end
