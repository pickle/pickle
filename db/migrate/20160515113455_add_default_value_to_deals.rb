class AddDefaultValueToDeals < ActiveRecord::Migration
  def change
    change_column :deals, :limited, :boolean, default: false
    change_column :deals, :original_qty, :integer, default: 0
    change_column :deals, :price, :float, default: 0.0
    change_column :deals, :deal_price, :float, default: 0.0
    change_column :deals, :app_post, :boolean, default: false
    change_column :deals, :bought_count, :integer, default: 0
    change_column :deals, :redeemed_count, :integer, default: 0
    change_column :deals, :app_buyable, :boolean, default: false
    change_column :deals, :cash_buyable, :boolean, default: false
    change_column :deals, :all_branches, :boolean, default: false
    change_column :deals, :current_qty, :integer, default: 0
    change_column :deals, :state, :boolean, default: false
    change_column :deals, :no_of_stamps, :integer, default: 0
  end
end