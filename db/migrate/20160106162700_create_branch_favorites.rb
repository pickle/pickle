class CreateBranchFavorites < ActiveRecord::Migration
  def change
    create_table :branch_favorites do |t|
      t.integer :branch_id
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
