class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :email
      t.string :mobile, :limit => 15
      t.string :encrypted_password
      t.integer :age
      t.string :gender, :limit => 10
      t.string :name
      t.integer :otp, :limit => 4
      t.boolean :otp_verified
      t.boolean :email_verified

      t.timestamps
    end
  end
end
