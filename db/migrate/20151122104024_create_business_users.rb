class CreateBusinessUsers < ActiveRecord::Migration
  def change
    create_table :business_users do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :designation
      t.string :mobile
      t.string :role
      t.string :username
      t.string :password_salt

      t.timestamps
    end
  end
end
