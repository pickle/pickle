class CreateBusinessesKeyword < ActiveRecord::Migration
  def change
    create_table :businesses_keywords do |t|
      t.references :business
      t.references :keyword

      t.timestamps
    end
  end
end
