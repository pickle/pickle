class AlterBusiness < ActiveRecord::Migration
  def change
  	rename_table :businesses,:branches
  	remove_column :branches, :category_id
  	remove_column :branches, :description
  	remove_column :branches, :website
  	remove_column :branches, :facebook
  	remove_column :branches, :keywords
  	rename_table :business_groups,:businesses
  	add_column :businesses, :category_id, :integer
  	add_column :businesses, :description, :text
  	add_column :businesses, :website, :string
  	add_column :businesses, :facebook, :string
  	add_column :businesses, :keywords, :string
  end
end
