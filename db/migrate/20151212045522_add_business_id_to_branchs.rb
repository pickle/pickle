class AddBusinessIdToBranchs < ActiveRecord::Migration
  def change
    add_column :branches, :business_id, :integer
  end
end
