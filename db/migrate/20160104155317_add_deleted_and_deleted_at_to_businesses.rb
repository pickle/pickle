class AddDeletedAndDeletedAtToBusinesses < ActiveRecord::Migration
  def change
    add_column :businesses, :deleted, :boolean
    add_column :businesses, :deleted_at, :datetime
  end
end
