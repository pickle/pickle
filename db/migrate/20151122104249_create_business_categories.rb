class CreateBusinessCategories < ActiveRecord::Migration
  def change
    create_table :business_categories do |t|
      t.string :name
      t.text :description
      t.string :image
      t.string :keywords

      t.timestamps
    end
  end
end
