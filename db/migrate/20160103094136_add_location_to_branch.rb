class AddLocationToBranch < ActiveRecord::Migration
  def change
    add_column :branches, :longitude, :string
    add_column :branches, :latitude, :string
  end
end
