class MoveLogoToBusiness < ActiveRecord::Migration
  def change
  	remove_column :branches, :logo
  	add_column :businesses, :logo, :string
  end
end
