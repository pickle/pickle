class CreateBranchViews < ActiveRecord::Migration
  def change
    create_table :branch_views do |t|
      t.integer :user_id
      t.integer :branch_id

      t.timestamps null: false
    end
  end
end
