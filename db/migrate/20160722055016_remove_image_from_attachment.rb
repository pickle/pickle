class RemoveImageFromAttachment < ActiveRecord::Migration
  def change
    remove_column :attachments, :image, :string
  end
end
