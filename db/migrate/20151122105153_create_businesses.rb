class CreateBusinesses < ActiveRecord::Migration
  def change
    create_table :businesses do |t|
      t.string :name
      t.integer :category_id
      t.text :description
      t.string :working_days
      t.time :open_time
      t.time :close_time
      t.text :address
      t.integer :location_id
      t.string :website
      t.string :twitter
      t.string :facebook
      t.string :keywords
      t.string :landline
      t.string :mobile
      t.boolean :state
      t.string :logo
      t.text :additional_info
      t.boolean :deleted
      t.datetime :deleted_at

      t.timestamps
    end
  end
end
