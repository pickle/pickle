class AddUsedToPunchedCardUsers < ActiveRecord::Migration
  def change
  	add_column :punched_card_users, :used, :boolean, :default => 0
  end
end
