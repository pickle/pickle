class RemoveNoOfOrdersFromDeals < ActiveRecord::Migration
  def change
    remove_column :deals, :no_of_orders, :integer
  end
end
