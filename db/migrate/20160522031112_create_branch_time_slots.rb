class CreateBranchTimeSlots < ActiveRecord::Migration
  def change
    create_table :branch_time_slots do |t|
      t.integer :branch_id
      t.integer :start_time, default: 0
      t.integer :end_time, default: 0
      t.string  :day, default: ''
      t.timestamps null: false
    end
  end
end
