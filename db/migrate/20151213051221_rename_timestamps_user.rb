class RenameTimestampsUser < ActiveRecord::Migration
  def change
  	rename_column :users, :updated_at, :updatedAt
  	rename_column :users, :created_at, :createdAt
  end
end
