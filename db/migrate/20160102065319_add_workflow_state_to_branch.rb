class AddWorkflowStateToBranch < ActiveRecord::Migration
  def change
    add_column :branches, :workflow_state, :string
  end
end
