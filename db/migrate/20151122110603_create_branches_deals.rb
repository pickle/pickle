class CreateBranchesDeals < ActiveRecord::Migration
  def change
    create_table :branches_deals do |t|
      t.integer :deal_id
      t.integer :branch_id
      t.integer :limited_no

      t.timestamps
    end
  end
end
