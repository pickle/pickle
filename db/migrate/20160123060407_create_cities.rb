class CreateCities < ActiveRecord::Migration
  def change
    create_table :cities do |t|
    	t.string :name
    	t.integer :country_id
      t.boolean :deleted, :default => false
      t.datetime :deleted_at
      t.timestamps null: false
    end
  end
end
