class ChangeDefaultValueForCashDealCodes < ActiveRecord::Migration
  def change
  	change_column :cash_deal_codes, :code, :string, :default => ''
  	change_column :cash_deal_codes, :status, :integer, :default => 0
  	change_column :cash_deal_codes, :code_status, :integer, :default => 0
  	change_column :cash_deal_codes, :bought_by, :integer, :default => 0
  	change_column :cash_deal_codes, :redeemed_by, :integer, :default => 0
  end
end
