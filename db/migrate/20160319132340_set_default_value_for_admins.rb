class SetDefaultValueForAdmins < ActiveRecord::Migration
  def change
    change_column :admins, :reset_password_token, :string, :default => nil
    change_column :admins, :invitation_token, :string, :default => nil
  end
end
