class AddPinCodeToAreas < ActiveRecord::Migration
  def change
  	add_column :areas, :pincode, :integer, :default => 111111
  end
end
