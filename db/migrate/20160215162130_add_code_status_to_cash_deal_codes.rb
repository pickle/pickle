class AddCodeStatusToCashDealCodes < ActiveRecord::Migration
  def change
  	add_column :cash_deal_codes, :status, :integer
  	add_column :cash_deal_codes, :code_status, :integer
  	add_column :cash_deal_codes, :bought_at, :datetime
  	add_column :cash_deal_codes, :redeemed_at, :datetime
  	add_column :cash_deal_codes, :bought_by, :integer
  	add_column :cash_deal_codes, :redeemed_by, :integer
  	remove_column :cash_deal_codes, :user_id
  end
end
