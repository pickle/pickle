class AddBuyTypeToDealCode < ActiveRecord::Migration
  def change
  	add_column :deal_codes, :buy_type, :integer, :default => 0
  end
end
