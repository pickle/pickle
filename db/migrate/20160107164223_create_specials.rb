class CreateSpecials < ActiveRecord::Migration
  def change
    create_table :specials do |t|
      t.string :title
      t.text :text
      t.string :image
      t.integer :business_id
      t.boolean :all_branches
      t.boolean :deleted
      t.datetime :deleted_at

      t.timestamps null: false
    end
  end
end
