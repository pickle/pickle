class AddMetaTypeToDeals < ActiveRecord::Migration
  def change
  	add_column :deals, :meta_text, :string, :default => ''
  end
end
