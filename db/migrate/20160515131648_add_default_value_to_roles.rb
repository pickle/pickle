class AddDefaultValueToRoles < ActiveRecord::Migration
  def change
    change_column :roles, :name, :string, default: ''
  end
end
