class CreateDeals < ActiveRecord::Migration
  def change
    create_table :deals do |t|
      t.string :name
      t.boolean :limited
      t.integer :limited_no
      t.float :price
      t.float :deal_price
      t.text :description
      t.date :start_date
      t.date :end_date
      t.string :available_days
      t.time :start_time
      t.time :end_time
      t.text :highlights
      t.text :terms_and_conditions
      t.text :redeem_steps
      t.text :additional_info
      t.integer :business_id
      t.boolean :state
      t.boolean :app_post
      t.integer :bought_count
      t.integer :redeemed_count
      t.boolean :app_buyable
      t.boolean :cash_buyable
      t.boolean :deleted
      t.datetime :deleted_at

      t.timestamps
    end
  end
end
