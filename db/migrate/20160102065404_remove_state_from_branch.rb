class RemoveStateFromBranch < ActiveRecord::Migration
  def change
  	remove_column :branches, :state
  end
end