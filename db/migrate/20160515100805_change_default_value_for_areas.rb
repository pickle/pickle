class ChangeDefaultValueForAreas < ActiveRecord::Migration
  def change
  	change_column :areas, :pincode, :integer, :default => 0
  end
end
