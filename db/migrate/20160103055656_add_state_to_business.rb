class AddStateToBusiness < ActiveRecord::Migration
  def change
    add_column :businesses, :state, :boolean
    remove_column :businesses, :workflow_state
  end
end
