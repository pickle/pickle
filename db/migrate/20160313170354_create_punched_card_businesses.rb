class CreatePunchedCardBusinesses < ActiveRecord::Migration
  def change
    create_table :punched_card_businesses do |t|
      t.references :deal
      t.integer :created_by
      t.references :business 
      t.string :stamp_code
      t.timestamps null: false
    end
  end
end
