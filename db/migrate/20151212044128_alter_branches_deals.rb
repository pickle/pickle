class AlterBranchesDeals < ActiveRecord::Migration
  def change
  	rename_column :branches_deals, :limited_no, :original_qty
  	add_column :branches_deals, :current_qty, :integer
  	add_column :branches_deals, :bought_count, :integer
  	add_column :branches_deals, :redeemed_count, :integer
  end
end
