class ChangeMobileColumnSize < ActiveRecord::Migration
  def change
  	change_column :users, :mobile, :string, :limit => 25
  end
end
