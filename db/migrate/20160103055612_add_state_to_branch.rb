class AddStateToBranch < ActiveRecord::Migration
  def change
    add_column :branches, :state, :boolean
    remove_column :branches, :workflow_state
  end
end
