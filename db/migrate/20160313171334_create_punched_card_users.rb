class CreatePunchedCardUsers < ActiveRecord::Migration
  def change
    create_table :punched_card_users do |t|
      t.integer :stamp_id
      t.integer :created_by
      t.boolean :current_stamp
      t.timestamps null: false
    end
  end
end
