class AddDefaultValueToUsers < ActiveRecord::Migration
  def change
    change_column :users, :age, :integer, default: 0
  end
end
