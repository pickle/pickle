class AddStampedByToPunchedCardUsers < ActiveRecord::Migration
  def change
  	add_column :punched_card_users, :stamped_by, :integer, default: 0
  	add_column :punched_card_users, :rewarded, :boolean, default: 0
  end
end
