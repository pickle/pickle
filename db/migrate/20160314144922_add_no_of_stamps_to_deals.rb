class AddNoOfStampsToDeals < ActiveRecord::Migration
  def change
    add_column :deals, :no_of_stamps, :integer
  end
end
