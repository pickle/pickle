class AlterDefaultImageDeleted < ActiveRecord::Migration
  def change
    change_column :images, :deleted, :boolean, :default => false
  end
end
