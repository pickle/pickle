class MoveTwitterToBusiness < ActiveRecord::Migration
  def change
  	remove_column :branches, :twitter
  	add_column :businesses, :twitter, :string
  end
end
