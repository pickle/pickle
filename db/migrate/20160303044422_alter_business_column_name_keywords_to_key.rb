class AlterBusinessColumnNameKeywordsToKey < ActiveRecord::Migration
  def change
    rename_column :businesses, :keywords, :key
  end
end
