class RemoveBranchAreaAndLocationFrom < ActiveRecord::Migration
  def change
  	remove_column :branches, :branch_area
  	remove_column :branches, :location_id
  end
end
