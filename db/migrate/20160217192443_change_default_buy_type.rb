class ChangeDefaultBuyType < ActiveRecord::Migration
  def change
  	change_column_default :deal_codes, :buy_type, 1
  end
end
