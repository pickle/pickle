class AddFieldsToDealCode < ActiveRecord::Migration
  def change
  	add_column :deal_codes, :bought_at, :datetime
  	add_column :deal_codes, :redeemed_at, :datetime
  	add_column :deal_codes, :bought_by, :integer
  	add_column :deal_codes, :redeemed_by, :integer
  	remove_column :deal_codes, :user_id
  end
end
