class CreateDealCodes < ActiveRecord::Migration
  def change
    create_table :deal_codes do |t|
    	t.integer :user_id
    	t.integer :branches_deal_id
    	t.string :code , :unique => true
    	t.integer :status , :default => 0
    	t.integer :code_status
      t.timestamps null: false
    end
  end
end
