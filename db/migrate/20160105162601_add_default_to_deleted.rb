class AddDefaultToDeleted < ActiveRecord::Migration
  def change
    %w(deals businesses branches categories).each do |i|
      change_column i.to_sym, :deleted, :boolean, :default => false
    end
  end
end
