class AddContactEmailToBranchBusiness < ActiveRecord::Migration
  def change
  	add_column :branches, :contact_email, :string, default: ''
  	add_column :businesses, :contact_email, :string, default: ''
  end
end
