class CreateCashDealCodes < ActiveRecord::Migration
  def change
    create_table :cash_deal_codes do |t|
    	t.integer :user_id
    	t.integer :branches_deal_id
    	t.string :code , :unique => true
      t.timestamps null: false
    end
    add_index :cash_deal_codes, [:user_id, :branches_deal_id], :unique => true
  end
end
