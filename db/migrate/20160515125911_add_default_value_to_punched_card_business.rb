class AddDefaultValueToPunchedCardBusiness < ActiveRecord::Migration
  def change
    change_column :punched_card_businesses, :stamp_code, :string, default: ''
  end
end
