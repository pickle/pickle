class ChangeUsers < ActiveRecord::Migration
  def change
  	add_index :users, :mobile, :unique => true
  	add_index :users, :email, :unique => true
  end
end
