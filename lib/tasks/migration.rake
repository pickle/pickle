namespace :migration do
  task :business_key_to_keywords => [:environment] do
    Business.find_each do |business|
      if business.key.present?
        business.key.split(',').each do |key|
          business.businesses_keywords.create(keyword: Keyword.find_or_create_by(name: key.strip.downcase))
        end
      end
    end
  end

  namespace :set_default_vaule do
    task :specials => [:environment] do
      Special.find_each do |special|
        if special.text.nil?
          special.text=''
          special.save!
        end
      end
    end

    task :areas => [:environment] do
      Area.find_each do |area|
        if area.pincode.nil?
          area.pincode=0
          area.save!
        end
      end
    end

    task :branches => [:environment] do
      Branch.find_each do |branch|
        if branch.open_time.nil?
          branch.open_time=0
        end
        if branch.close_time.nil?
          branch.close_time=0
        end
        if branch.state.nil?
          branch.state=0
        end
        branch.save!
      end
    end

    task :branches_deals => [:environment] do
      BranchesDeal.find_each do |branches_deals|
        if branches_deals.original_qty.nil?
          branches_deals.original_qty=0
        end
        if branches_deals.current_qty.nil?
          branches_deals.current_qty=0
        end
        if branches_deals.bought_count.nil?
          branches_deals.bought_count=0
        end
        if branches_deals.redeemed_count.nil?
          branches_deals.redeemed_count=0
        end
        begin
          branches_deals.save
        rescue Exception => e
          p branches_deals.id.to_s + "=======>>>>>>" 
          p e
        end
        
      end
    end

    task :businesses => [:environment] do
      Business.find_each do |business|
        if business.state.nil?
          business.state=0
        end
        business.save!
      end
    end
    
    task :cash_deal_codes => [:environment] do
      CashDealCode.find_each do |cash_deal_codes|
        if cash_deal_codes.code.nil?
          cash_deal_codes.code=''
        end
        if cash_deal_codes.status.nil?
          cash_deal_codes.status=0
        end
        if cash_deal_codes.code_status.nil?
          cash_deal_codes.code_status=0
        end
        if cash_deal_codes.bought_by.nil?
          cash_deal_codes.bought_by=0
        end
        if cash_deal_codes.redeemed_by.nil?
          cash_deal_codes.redeemed_by=0
        end
        begin
          cash_deal_codes.save
        rescue Exception => e
          p cash_deal_codes.id + "=======>>>>>>" + e
        end
      end
    end

    task :countries => [:environment] do
      Country.find_each do |country|
        if country.name.nil?
          country.name=''
        end
        country.save!
      end
    end

    task :deals => [:environment] do
      Deal.find_each do |deal|
        deal.limited=false      if deal.limited.nil?
        deal.original_qty=0     if deal.original_qty.nil?
        deal.price=0            if deal.price.nil?
        deal.deal_price=0       if deal.deal_price.nil?
        deal.app_post=false     if deal.app_post.nil?
        deal.bought_count=0     if deal.bought_count.nil?
        deal.redeemed_count=0   if deal.redeemed_count.nil?
        deal.app_buyable=false  if deal.app_buyable.nil?
        deal.cash_buyable=false if deal.cash_buyable.nil?
        deal.all_branches=false if deal.all_branches.nil?
        deal.current_qty=0      if deal.current_qty.nil?
        deal.state=false        if deal.state.nil?
        deal.no_of_stamps=0     if deal.no_of_stamps.nil?
        deal.save!
      end
    end

    task :keywords => [:environment] do
      Keyword.find_each do |keyword|
        if keyword.name.nil?
          keyword.name=''
        end
        keyword.save!
      end
    end

    task :permissions => [:environment] do
      Permission.find_each do |permission|
        permission.name=''            if permission.name.nil?
        permission.subject_class=''   if permission.subject_class.nil?
        permission.action=''          if permission.action.nil?
        permission.save!
      end
    end

    task :punched_card_businesses => [:environment] do
      PunchedCardBusiness.find_each do |punched_card_business|
        if punched_card_business.stamp_code.nil?
          punched_card_business.stamp_code=''      
          punched_card_business.save!
        end
      end
    end

    task :ratings => [:environment] do
      Rating.find_each do |rating|
        if rating.rating.nil?
          rating.rating=0.0
          rating.save!
        end
      end
    end

    task :roles => [:environment] do
      Role.find_each do |role|
        if role.name.nil?
          role.name='' 
          role.save!
        end
      end
    end

    task :users => [:environment] do
      User.find_each do |user|
        if user.age.nil?
          user.age=0
          user.save!
        end
      end
    end

  end
end