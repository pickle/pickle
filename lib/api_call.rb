module ApiCall

  def protocol
    Gateway.protocol
  end

  def endpoint
    Gateway.endpoint
  end

  def access_token
    Gateway.access_token.to_s
  end

  def uid
    Gateway.uid.to_s
  end

  def write(method, path, parameters={})
    conn = prepare_conn(path)
    p "====="
    p parameters
    response = conn.send(method) do |req|
      req.headers['Content-Type'] = 'application/json'
      req.headers['x-access-token'] = access_token
      req.headers['uid'] = uid
      req.body = JSON.generate(parameters, quirks_mode: true)  if parameters.present?
      p "====="
      req.body
    end
    out = JSON.parse(response.body)
    p out
    out
  end

  def prepare_conn(path)
    url = "#{protocol}://#{endpoint}/#{path}"
    url = URI.encode(url)
    conn = Faraday.new(:url => url) do |faraday|
      faraday.request  :url_encoded             # form-encode POST params
      faraday.response :logger                  # log requests to STDOUT
      faraday.adapter  Faraday.default_adapter  # make requests with Net::HTTP
    end
    conn
  end
end
