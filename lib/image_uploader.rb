module ImageUploader

  def to_cloudinary(path)
    Cloudinary::Uploader.upload(path)
  end

end