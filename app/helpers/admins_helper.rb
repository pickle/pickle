module AdminsHelper

  def invitation_accepted?(admin)
    admin.invitation_accepted_at.present?
  end

end
