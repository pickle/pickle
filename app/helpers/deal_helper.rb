module DealHelper

  def seconds_to_time(t)
    Time.at(t).utc.strftime("%I:%M%P")
  end

  def deal_live?(deal)
    html = ""
    if deal.buy_end_date
      if (Date.today <= deal.buy_end_date) and
          Time.now.seconds_since_midnight.to_i.between?(deal.start_time.to_i,deal.end_time.to_i) and
          (deal.send(Date.today.strftime("%a").downcase.to_s) == true)
        html = '<i class="fa fa-circle text-flat"></i>'
      else
        html = '<i class="fa fa-circle text-warning"></i>'
      end
    end
    html.html_safe
  end

  def calc_active(p_date,w_hsh_key,i)
    if p_date.nil? && i == 0
      true
    else
      p_date == w_hsh_key
    end
  end

  def day_pill(text,href, active = false, month)
    html = ''
    html += '<div class="col-xs-1">
        <a class="block block-link-hover3 text-center " href="' + href + '">
          <div class="block-content block-content-full ' + (active ? "bg-flat" : "") + '">
          <small>' + month + '</small>
            <div class="font-w600 push-15-t ' + (active ?  "text-white" : "") + '">' + text + '</div>
          </div>
        </a>
      </div>'

    html.html_safe
  end

  def dir_pill(dir,href)
    html = ''
    html += '<div class="col-xs-1">
      <a class="block block-link-hover3 text-center" href="' +  href  +'">
        <div class="block-content block-content-full">
          <i class="fa fa-chevron-' + dir + ' fa-4x text-warning"></i>
        </div>
      </a>
    </div>'
    html.html_safe
  end

  def deal_color(deal)
    if !deal.buy_end_date.blank?
      if deal.buy_end_date < Date.today
        "<span class='label label-danger'>Expired</span>"
      elsif deal.buy_end_date == Date.today
        "<span class='label label-primary'>Today</span>"
      elsif deal.buy_end_date > Date.today
        "<span class='label label-success'>Active</span>"
      else
        "<span class='label label-info'>Regular</span>"
      end
    else
      ""
    end
  end

end
