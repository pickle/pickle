module ImageHelper
  def mini_box(icon,sign,no,text)
    html = ""
    html += '<div class="col-xs-3">
        <a class="block block-link-hover3" href="javascript:void(0)">
          <table class="block-table text-center">
            <tbody>
              <tr>
                <td style="width: 50%;">
                  <div class="push-30 push-30-t">
                    <i class="si si-' + icon + ' fa-3x text-primary"></i>
                  </div>
                </td>
                <td class="bg-gray-lighter" style="width: 50%;">
                  <div class="h3 font-w700"><span class="h2 text-muted">' + sign +'</span> ' + no +  '</div>
                  <div class="h5 text-muted text-uppercase push-5-t">' + text +'</div>
                </td>
              </tr>
            </tbody>
          </table>
        </a>
      </div>'
    html.html_safe
  end

end
