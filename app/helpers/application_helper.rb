module ApplicationHelper

  def link_to_add_fields(name, f, type)
    new_object = f.object.send "build_#{type}"
    id = "new_#{type}"
    fields = f.send("#{type}_fields", new_object, child_index: id) do |builder|
      render(type.to_s + "_fields", f: builder)
    end
    link_to(name, '#', class: "add_fields btn btn-dm btn-warning", data: {id: id, fields: fields.gsub("\n", "")})
  end


  # bs -stands for bootstrap
  def bs(f,n,elem,lab,div,div2 = nil,div3 = nil, mand = nil)
    if lab[0].nil?
      l = n.is_a?(Array) ? n[0].to_s : n.to_s
    else
      l = lab[0]
    end

    if n.kind_of?(Array)
      n.last[:class] = n.last[:class].present? ? (n.last[:class] + " form-control") : "form-control"
      name = n
    else
      name = [n,class: "form-control"]
    end

    div2_head = div2.nil? ? "" : "<div class='col-xs-#{div2}'>"
    div2_tail = div2.nil? ? "" : "</div>"

    div3_head = div3.nil? ? "" : "<div class='#{div3}'>"
    div3_tail = div3.nil? ? "" : "</div>"

    (div3_head +
     div2_head +
     '<div class="form-group">' +
     f.label((name.kind_of?(Array) ? n[0].to_sym : n.to_sym),
             l.titleize,
             {class: "control-label col-xs-" + lab[1].to_s }) +
     "<div class=col-xs-#{div} >" +
     f.send(elem,*name) +
     "</div>" +
     "</div>" +
     div2_tail +
     div3_tail).html_safe

  end

  def form_element(field,f,element,name,elem_size = 10, no_element = true, mandate = false, grid = false, show_label=true)
    field, actual_label = (field.is_a?(Array) ? field : [field,field])
    mandate_text = '<span class="red">&nbsp;*</span>'
    html = ""
    html += '<div id="field_' + field  + '"  class="form-group'+ (grid ? ' col-md-6' : ' col-md-12') +'">'
    if show_label
      html += '<label class="col-xs-12" for="example-text-input">' + actual_label.titleize + (mandate ? mandate_text : '') + '</label>'
    end
    html += ''
    html += '<div class="col-md-' + elem_size.to_s  + ' ' + field.to_s +  '">'
    html += f.send(element,field,*name) if no_element
    html += '</div></div>'
    html.html_safe
  end
end
