module CategoriesHelper
  def parent(category)
    category.parent_id.present? ? Category.find_by(id: category.parent_id).name : "-"
  end
end