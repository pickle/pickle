class WeekView
  def initialize(date = nil)
    temp = date || Date.today
    @current = temp.to_date
  end

  def get_week_hsh
    hsh = {}
    (@current..(@current + 9)).each do |day|
      hsh[day.strftime("%a %d")] = [day.to_s,day.strftime("%b")]
    end
    hsh
  end

  def self.deals(d,m = nil)
    model = m || Deal
    dt = d.to_date
    model.where("DATE(deals.end_date) > '#{dt}' AND deals.#{dt.strftime("%a").downcase} = 1").order("end_time ASC")
  end

end

