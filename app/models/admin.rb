class Admin < ActiveRecord::Base

  # https://github.com/ryanb/cancan/wiki/Role-Based-Authorization
  ROLES = %w[admin sub_admin sales marketing]
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :invitable, :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validates :email, uniqueness: true
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :save }

  has_and_belongs_to_many :businesses

  #role can have any name from now
 # before_save :check_role

  def check_role
    ROLES.include?(self.role)
  end

  def admin?
    role == 'admin'
  end

  def sub_admin?
    role == 'sub_admin'
  end

  def sales?
    role == 'sales'
  end

  def marketing?
    role == 'marketing'
  end  

end
