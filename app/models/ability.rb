class Ability
  include CanCan::Ability

  def initialize(user)
    if user
      role_names = user.role.split(",")
      roles = Role.where(name: role_names)
      roles.each do |r|
        can :manage, :all if r.name == "admin"
        r.permissions.each do |permission|
          if permission.subject_id.nil?
            can permission.action.to_sym, permission.subject_class.constantize
          elsif permission.subject_id == 1
            can permission.action.to_sym, permission.subject_class.constantize, users: { id: user.id }
          end
        end
      end
    end
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end
