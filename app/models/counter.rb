class Counter < ActiveRecord::Base

  def self.branches(deals)
    deals.joins(:branches_deals).group("branches_deals.deal_id").count
  end

end
