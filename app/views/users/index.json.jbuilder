json.array!(@users) do |user|
  json.extract! user, :id, :email, :mobile, :age, :gender, :name, :otp, :otp_verified, :email_verified, :dob, :authtoken, :createdAt, :updatedAt
  json.url user_url(user, format: :json)
end
