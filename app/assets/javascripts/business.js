$(function(){
  $("#business_logo").focusout(function(){
    var url = $(this).val();
    $("div.logo_preview").html("<img height=100 src=" + url + ">");
  });
  image_add_cb(".business_image");
  add_keywords();
  $("#business_terms_and_conditions, #business_redeem_steps").wysihtml5({'toolbar': {'blockquote': false, 'html': true,'image' : false}});

});

// nested form on add event
$(document).on('nested:fieldAdded', function(event){
  image_add_cb(".business_image");
  check_and_remove_add(event.field, ".business_images",4);
});


function cloudinary_check(url){
  if (url.search("cloudinary") !== -1){
    notys("Nice.. you found a cloudinary url for your logo!","success");
  }
  else{
    notys("Are you sure, this is a cloudinary url?","warning");
  }
}

function add_keywords(){
  $("#business_add_keywords").tokenInput("/search_keywords");

}

