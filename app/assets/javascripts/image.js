function image_cloudinary_upload(){
  $('.image_upload').unsigned_cloudinary_upload("ovkrdjbz",
                                                { cloud_name: 'getpickledeals' }
                                               ).bind('fileuploadchange', function(e,data) {
                                                 window.count = data.files.length;
                                                 window.chk = 0;
                                                 $('#upload_status').html("Uploading to Cloudinary... <i class='si si-cloud-upload'></i>")
                                                 $(this).hide();
                                               }).bind("cloudinarydone", function(e, data){
                                                 var image_text = $(this).parents(".fields").find(".deal_image");
                                                 image_text.val(data.result.url);
                                                 image_text.focusout();
                                                 window.chk+=1
                                                 if(window.chk==window.count) { $('#upload_status').html("Upload Done <i class='si si-like'></i>");  }
                                               });
                                               $(".cloudinary_fileupload").addClass("form-control");
}

function image_cloudinary_upload_generic(parent,field){
  $('.image_upload').unsigned_cloudinary_upload("ovkrdjbz",
                                                { cloud_name: 'getpickledeals' }
                                               ).bind('fileuploadchange', function(e,data) {
                                                 window.count = data.files.length;
                                                 window.chk = 0;
                                                 $(field).parents('.form-material').find('.cloudinary-upload-status').html("Uploading to Cloudinary... <i class='si si-cloud-upload'></i>")
                                                 $(this).hide();
                                               }).bind("cloudinarydone", function(e, data){
                                                 var image_text = $(this).parents(parent).find(field);
                                                 image_text.val(data.result.url);
                                                 image_text.focusout();
                                                 window.chk+=1
                                                 if(window.chk==window.count) {
                                                   $(field).parents('.form-material').find('.cloudinary-upload-status').html("Upload Done <i class='si si-like'></i>");
                                                 }
                                               });
                                               $(".cloudinary_fileupload").addClass("form-control");
}

