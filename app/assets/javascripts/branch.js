$(function(){
  initializeAutocomplete();
  $("#branch_branch_open_time,#branch_branch_close_time").timepicker();
  $(".branch-time").timepicker();
  image_add_cb(".branch_image");
  image_add_cb(".branch_attachment_image");
  $("#branch_logo").focusout(function(){
    var url = $(this).val();
    $("div.logo_preview").html("<img height=100 src=" + url + ">");
  });

  $('.add-timing-common').click(function() {
    $(this).hide();
    var currentSection = $(this).parent();
    currentSection.find('.common-time-second').show();
    currentSection.find('.common-time-second').find('input').removeAttr('disabled');
    currentSection.find('.remove-timing-common').show();
  });

  $('.remove-timing-common').click(function() {
    $(this).hide();
    var currentSection = $(this).parent();
    currentSection.find('.common-time-second').hide();
    currentSection.find('.common-time-second').find('input').attr('disabled', 'disabled');
    currentSection.find('.add-timing-common').show();
  });

  $('#add-timing-head').click(function() {
    $(this).hide();
    $('#remove-timing-head').show();
    $('#common-time-second-head').show();
    $('#common-time-second-head').find('input').removeAttr('disabled');
    $('.add-timing-common').click();
  });

  $('#remove-timing-head').click(function() {
    $(this).hide();
    $('#add-timing-head').show();
    $('#common-time-second-head').hide();
    $('#common-time-second-head').find('input').attr('disabled', 'disabled');
    $('.remove-timing-common').click();
  });

  $('.branch-time').change(function() {
    $(this).parents('.time-slot-item').children('.switch').children('input').val('1')
  })

  $('.fa-sort').each(function() {
    var prevElem = $(this).prev().text();
    if (prevElem.match('▲') || prevElem.match('▼')) {
      $(this).hide();
      return false;
    }
  });
});

// nested form on add event
$(document).on('nested:fieldAdded', function(event){
  image_add_cb(".branch_image");
  image_add_cb(".branch_attachment_image");
  // check_and_remove_add(event.field, ".branch_images",4);
  var attachmentId = $($(event)[0].target).find('.cloudinary-url-field').attr('id');
  image_cloudinary_upload_generic(".fields", '#'+attachmentId);
});

function initializeAutocomplete(){
  var input = document.getElementById('branch_address');
  var options = {
    componentRestrictions: {country: "IN"}
  };

  var autocomplete = new google.maps.places.Autocomplete(input, options);

  google.maps.event.addListener(autocomplete, 'place_changed', function() {
    var place = autocomplete.getPlace();
    var lat = place.geometry.location.lat();
    var lng = place.geometry.location.lng();
    var placeId = place.place_id;
    // to set city name, using the locality param
    var componentForm = {
      locality: 'short_name',
    };
    document.getElementById("branch_latitude").value = lat;
    document.getElementById("branch_longitude").value = lng;
  });
}

