function initSpecialAutoComplete(){
  $('.autocomplete_special').autocomplete({
    minLength: 1,
    source: function(request, response) {
      $.ajax({
        url: "/businesses/autocomplete",
        dataType: "json",
        data: {q: request.term},
        success: function( data ) {
          response( data );
        }
      });
    },
    select: function( event, ui ) {
      $("#special_business_id").val( ui.item.id );
      all_branches_toggle();
    },
    focus: function( event, ui ) {
      $( "#auto_search_complete" ).val( ui.item.label );
      return false;
    }
  }).autocomplete( "instance" )._renderItem = function( ul, item ) {
    return $( "<li>" )
    .append( "<a>" + item.id + " : " +  item.label + "</a>" )
    .appendTo( ul );
  };
}

$(function(){
  $("#special_image").focusout(function(){
    var url = $(this).val();
    $("div.image_preview").html("<img height=100 src=" + url + ">");
  });
});

function specials_edit(){
    if(!$("#special_all_branches").is(":checked")) {
      special_all_branches("edit");
      check_branches();
    }
    else{
      $('#branches').empty();
      $('#branches').hide();
      $('#search_branch').hide();
    }
}

function all_branches_toggle(){
  $('#all_branches').show();

  $('#special_all_branches').change(function() {
    if(!$(this).is(":checked")) {
      special_all_branches("new");
    }
    else{

      $('#branches').empty();
      $('#branches').hide();
      $('#search_branch').hide();

    }
  });
}

function special_all_branches(type){
  $('#branches').show();
  $('#search_branch').show();

  var business_name = $("#business_name").val();
  var url = "/branches/search/"
  $.ajax({
    type: "GET",
    dataType: "json",
    url: url,
    data: {q: business_name },
    success: function(data){
      var branches = data.branches;
      if (branches !== "undefined" || branches !== null || branches !== "") {
        $.each(branches, function(i, item){
          $("#branches").append("<div class='col-md-10'><p value="+item[0]+">" +item[1]+ " - " + item[2] +"</div><div class='col-md-2'> <input class='form-control' type='checkbox' id="+item[0]+" name='special[branches_specials][]' value="+item[0]+" autocomplete=off /> </p></div>");
        });
      }
      if (type == "edit"){
        check_branches();
      }
    }
  });
}

function check_branches(){
  var special_id = $("#page_header").data("id");
  $.get("/special_branch_ids/" + special_id,function(out) {
    $.each(out, function(i,item){
      $("#branches input#" + item).attr("checked","checked");
    });
  });
}
