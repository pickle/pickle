// nested form on add event

$(document).on('nested:fieldAdded', function(event){
  image_add_cb(".deal_image");
  // senthil: 2016apr9: removed to persist add image button
  // check_and_remove_add(event.field, ".deal_images",4);
  image_cloudinary_upload_generic('.form-material', '.deal_image');
});

function characterCount(e) {
    var currentCount = $(this).val().length;
    var maxCount = $(this).parent().find('.text-counter').attr('data-max-count');
    $(this).parent().find('.text-counter').text(currentCount + '/' + maxCount);
    if (parseInt(currentCount) == parseInt(maxCount) && (e.keyCode != 8)) {
      return false;
    }
}

function addCounter(identifier, count) {
  if ($(identifier).length > 0) {
    var currentCount = $(identifier).val().length;
    $(identifier).parent().prepend('<div class="text-counter" data-max-count="' + count + '">' + currentCount + '/' + count + '</div>');
    $(identifier).keyup(characterCount);
    $(identifier).keydown(characterCount);
  }
}

function toggle_deal_type(){
  $("#deal_deal_type").change(function(){
    var val = $(this).val();
    if (val == "info"){
      $("#field_price").hide();
      $("#field_deal_price").hide();
      $("#deal_info_meta_text").show();
      $("#deal_no_of_stamps").hide();
      $("#field_code_expiry_date").hide();
      $('.payment-quantity-section').hide();
    }else if (val == "regular"){
      $("#field_price").show();
      $("#field_deal_price").show();
      $("#deal_info_meta_text").hide();
      $("#deal_no_of_stamps").hide();
      $("#field_code_expiry_date").show();
      $('.payment-quantity-section').show();
    }else if (val == "punched_cards"){
      $("#field_price").hide();
      $("#field_deal_price").hide();
      $("#deal_info_meta_text").hide();
      $("#deal_no_of_stamps").show();
      $("#field_code_expiry_date").hide();
      $('.payment-quantity-section').hide();
    }

  });
}

function populate_prefill_text(ui){
  $.get("/get_prefill_text/" + ui.item.id, function(data){
    // $("#deal_redeem_steps").data("wysihtml5").editor.setValue(data["redeem_steps"]);
  });
}

function choose_autocomplete(ui,type){
  $('#branches').html("");

  if (type == "new"){
    $('#deal_all_branches').prop('checked', true);
    $('#deal_limited').prop('checked', false);
    $("#deal_business_id").val(ui.item.id)
    $('#deal_original_qty').val(0);
  }

  $("#all_branches").show();

  $("#limited").show();
}
function toggle_limited_unchecked(){
  $('#count_box').hide();
  $('input[name^="deal[branches]"]').attr('type','checkbox')
}

function toggle_limited_checked(){
  if($('#deal_all_branches').is(":checked")){
    $('#count_box').show();
  }
}

function toggle_all_branches_checked(){
  $('#branches').html("");
  $('#search_branch').html("");
  $('#search_branch').hide();
  $('#branches').empty();
  if($('#deal_limited').is(":checked")){
    $('#count_box').show();
  }
  else{
    $('#count_box').emtpy();
  }
}

function toggle_all_branches_unchecked(business_name){
  $('#branches').show();
  $('#search_branch').show();
  $('#deal_original_qty').val(0);
  $('#count_box').hide();

  $("#branches").append('<div class="row"><div class="col-md-1"><input type="checkbox" name="deal[all_branches]" value="1" id="deal_all_branches" class="branch-deal-check form-control" data-value="all"></div><div class="col-md-3"><div class="deals-branch-list">All Branches</div></div><div class="col-md-2"><input class="form-control" type="number" value="0" name="deal[original_qty]" autocomplete="off" id="deal_original_qty" style="display: none;"></div></div>')
  var business_name = business_name || $("#deal_business_name").val();
  var page_type = $("#page_header").data("type");

  if (page_type == "edit"){
    var deal_id = $("#page_header").data("value");
    var url = "/branches/branches_deals/"
    $.ajax({
      type: "GET",
      dataType: "json",
      url: url,
      data: {q: business_name, deal_id: deal_id},
      success: function(data){
        if (data !== "undefined" || data !== null || data !== "") {
          $.each(data, function(i, item){
            var checkStatus = '';
            var value = item[2];
            if (value > 0 || value == null) {
              checkStatus = 'checked="checked"';
            }
            $("#branches").append("<div class='row'>"+
              "<div class='col-md-1'><input class='form-control branch-deal-check' type='checkbox' value='0' name='deal[branches]["+item[0]+"]' autocomplete='off' data-value='"+item[0]+"' " + checkStatus +"></div>"+
              "<div class='col-md-3'><div value="+item[0]+" class='deals-branch-list'>" +item[1]+ " - " + item[3] + "</div></div>"+
              "<div class='col-md-2'> <input class='form-control' type='number' value='"+ value +"' name='deal[branches]["+item[0]+"]' id='deal_branches_" + item[0] + "' autocomplete=off style='display: none;' disabled='disabled'/></div>"+
              "<div class='col-md-6 branch-error' id='branch_error_" + item[0] +"'></div></div>");
          });
          if(!$("#deal_limited").is(":checked")){
            $('input[name^="deal[branches]"]').attr('type','checkbox')
          }

          $('#deal_business_name').attr('disabled', 'disabled');
          $('.payment-quantity-section').show();
          $('.branch-deal-check').change(branch_check_handler);
          $('.branch-deal-check:checked').removeAttr('name');
          $('.branch-deal-check:checked').change();

          if (isAllBranches) {
            $('#deal_all_branches').prop('checked', true);
            $('#deal_all_branches').change();
          }

        }
      }
    });

  }
  else{

    var url = "/branches/search/"
    $.ajax({
      type: "GET",
      dataType: "json",
      url: url,
      data: {q: business_name },
      success: function(data){
        var branchData = data.branches;
        if (branchData !== "undefined" || branchData !== null || branchData !== "") {
          $.each(branchData, function(i, item){
            $("#branches").append("<div class='row'>"+
              "<div class='col-md-1'><input class='form-control branch-deal-check' type='checkbox' value='0' name='deal[branches]["+item[0]+"]' autocomplete='off' data-value="+item[0]+" onClick='validateBranchTimings()'></div>"+
              "<div class='col-md-3'><div class='deals-branch-list' value="+item[0]+">" +item[1]+ " - " + item[2] +"</div></div>"+
              "<div class='col-md-2'><input class='form-control' type='number' value='0' name='deal[branches]["+item[0]+"]' id='deal_branches_" + item[0] + "' autocomplete=off style='display: none;' disabled='disabled'/></div>"+
              "<div class='col-md-6 branch-error' id='branch_error_" + item[0] +"'></div></div>");
          });
          // $('.branch-deal-check').change(validateBranchTimings);
          if(!$("#deal_limited").is(":checked")){
            $('input[name^="deal[branches]"]').attr('type','checkbox');
            $('.branch-deal-check').change(branch_check_handler);
          }
        }
        $('#deal_terms_and_conditions').froalaEditor('html.set', data.business_terms);
      }
    });
  }

}

function branch_check_handler() {
  var branchId = $(this).attr('data-value');

  var iden = $('#deal_branches_' + branchId);
  if (branchId == 'all') {
    iden = $('#deal_original_qty');
  }

  if ($(this).is(':checked') && $('#deal_limited').is(':checked')) {
    iden.attr('type','number');
    iden.removeAttr('disabled');
    iden.show();
    $(this).removeAttr('name');
  } else if ($(this).is(':checked') &&  !$('#deal_limited').is(':checked')) {
    iden.attr('type','checkbox').val(1);
    iden.removeAttr('disabled');
    iden.hide();
  } else {
    iden.val('');
    iden.attr('disabled', 'disabled');
    iden.hide();
    $(this).attr('name', iden.attr('name'));
  }

  if (branchId == 'all' && $('#deal_all_branches').is(':checked')) {
    $('.branch-deal-check').prop('checked', true);
    // $('input[name^="deal[branches]"').attr('disabled', 'disabled').hide();
  } else if (!$('#deal_all_branches').is(':checked') && branchId == "all") {
    $('.branch-deal-check').prop('checked', false);
  } else if ($('#deal_all_branches').is(':checked') && branchId != "all") {
    $('.branch-deal-check').prop('checked', true);
  }
}

function new_deal_assorted(){
  $("#valid_on input").prop('checked', true);
  $("#switches input").prop('checked', true);
  $("#deal_deal_start_time,#deal_deal_end_time").val('');
}

function build_preview_data() {
  var title = $('#deal_name').val();
  var imgUrl = $('#deal_images_attributes_0_original').val();
  var description = $('#deal_description').val();
  var useBetween = '';
  var redeemType = $('.redeem-type-selection:checked').data('redeem-type');
  if (redeemType == 'time') {
    var startTime = $('#deal_deal_start_time').val();
    var endTime = $('#deal_deal_end_time').val();
    useBetween = startTime + ' - ' + endTime;
  } else if (redeemType == 'date') {
    var startDate = $('#deal_start_date').val();
    var endDate = $('#deal_end_date').val();
    useBetween = startDate + ' - ' + endDate;
  } else if (redeemType == 'working_hours') {
    useBetween = 'Working Hours';
  } else {
    useBetween = 'See Description';
  }
  var validDays = []
  $('.switch-warning > input[type="checkbox"]:checked').each(function() {
    validDays.push($(this).attr('id').replace('deal_', ''));
  });
  validDays = validDays.map(function(day) {
    return day.replace(/\b[a-z]/g, function(letter) {
        return letter.toUpperCase();
    });
  }).join(', ');
  var offerEnd = $('#deal_buy_end_date').val();
  var addlInfo = $('#deal_additional_info').val();
  var terms = $('#deal_terms_and_conditions').val();
  var redeemSteps = $('#deal_redeem_steps').val();

  $('#preview_title').text(title);
  $('#preview_details').text(description);
  $('#preview_image').attr('src', imgUrl);
  $('#preview_valid_on').text(validDays);
  $('#preview_use_between').text(useBetween);
  $('#preview_end').text(offerEnd);
  $('.redeem-product-description').html('<div class="text-center"><strong>' + title +'</strong></div>'+
      '<div class="push-10-t">'+
      addlInfo +
      '</div>');
  $('.redeem-product-terms').html(terms);
  $('.redeem-product-how-to').html(redeemSteps);
}

function validateBranchTimings() {
  var branches = []
  var deal_time = []
  var checkedBranchesDom = $('.branch-deal-check:checked').not('#deal_all_branches');
  if (checkedBranchesDom.length == 0 || $('#deal_all_branches').is(':checked')) {
    return;
  }
  checkedBranchesDom.each(function(k, v) {
    branches.push($(v).data('value'));
  });

  var validDays = []
  $('.switch-warning > input[type="checkbox"]:checked').each(function() {
    validDays.push($(this).attr('id').replace('deal_', ''));
  });

  var dealStart = $('#deal_deal_start_time').val();
  var dealEnd = $('#deal_deal_end_time').val();
  if (dealStart && dealEnd) {
    deal_time.push(dealStart);
    deal_time.push(dealEnd);
  }
  $.ajax({
    url: "/deals/validate-timings",
    dataType: "json",
    method: "post",
    data: {branch_ids: branches, deal_time: deal_time, days: validDays},
    success: function( data ) {
      var errMsg = {
        d: 'Branch open days do not match "Valid" days selected above',
        t: 'Branch working hours do not match "Use Between" hours above'
      }
      jQuery.each(data, function(k, v) {
        $('#branch_error_' + v.branch).text(errMsg[v.type]);
      });
    }
  });
}


$(function(){

  $('.code_expiry_date').parent().find('label').append('&nbsp;&nbsp;<i class="fa fa-info-circle" data-toggle="tooltip" title="Code Expiry is the date when the 4-digit code expires for users who have purchased the offer"></i>')
  $('[data-toggle="tooltip"]').tooltip();

  $('.deal-type option:first').attr('disabled', 'disabled');
  $('.deal-type option:first').removeAttr('checked');

  $('.deal-type').change(function() {
    if ($(this).val().length > 0) {
      $('.deal-basic-main').slideDown();
    } else {
      $('.deal-basic-main').slideUp();
    }
  });

  $('#preview_tab').click(build_preview_data);
  $('.wizard-next').click(function() {
    if ($('.nav-tabs li.active a')[0].hash == '#tab-more') {
      build_preview_data();
    }
  })

  addCounter('#deal_name', 30)
  addCounter('#deal_description', 125)
  addCounter('#deal_meta_text', 50)

  $('.redeem-type-selection').change(function() {
    var redeemType = $(this).data('redeem-type');
    var currentItems = $(this).parents('.redeem-type-item').find('.redeem-type-input');
    $('.redeem-type-input').attr('disabled', 'disabled');
    currentItems.prop('disabled', !$(this).prop('checked'));
    $('.redeem-type-selection').not(this).prop('checked', false);
    $('#deal_redeem_type').val($(this).data('redeem-type'));
  });

  $('.redeem-type-selection').change();

  $('#deal_cash_buyable').change(function() {
    if ($(this).prop('checked')) {
      $('#deal_limited').val(0);
      $('#deal_limited').attr('disabled', 'disabled');
      $('#deal_limited').parent().removeClass('switch-success');
    } else {
      $('#deal_limited').removeAttr('disabled');
      $('#deal_limited').parent().addClass('switch-success');
    }
  });

  $('#deal_cash_buyable').change();

  image_cloudinary_upload();
  toggle_deal_type();
  initAutoComplete();

  $('.switch-warning input[type="checkbox"]').change(validateBranchTimings);
  $('#deal_deal_start_time,#deal_deal_end_time').focusout(validateBranchTimings);

  var nowDate = new Date();
  var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);

  $(".js-weekview-datepicker").datepicker({multidateSeparator: ";", startDate: today}).on('changeDate', function (ev) {
    $.get("/get_ruby_date/" + ev.date,function(data){
      window.location.href = "/deals/week_view/" + data + "/" + data;
    });
    // var dt = new Date(ev.date);
    // var dd = dt.getDate();
    // var mm = dt.getMonth();
    // var yyyy = dt.getFullYear();
    // var fulldt = yyyy + "-" + mm + "-" + dd;
  });

  image_add_cb(".deal_image");

  $(".js-datepicker").datepicker('option','dateFormat','yy-mm-dd')

  $('#deal_buy_start_date').on('changeDate', function(selected) {
    var startDate = new Date(selected.date.valueOf());
    startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
    $('#deal_buy_end_date').datepicker('setStartDate', startDate);
    $('#deal_code_expiry_date').datepicker('setStartDate', startDate);
  });

  $('#deal_buy_end_date').on('changeDate', function(selected) {
    var startDate = new Date(selected.date.valueOf());
    startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
    $('#deal_code_expiry_date').datepicker('setStartDate', startDate);
  });

  $('#deal_start_date').on('changeDate', function(selected) {
    var startDate = new Date(selected.date.valueOf());
    startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
    $('#deal_end_date').datepicker('setStartDate', startDate);
  });

  $("#deal_deal_start_time,#deal_deal_end_time").timepicker();

  $("a#check_all_day").click(function(){

    $("#deal_deal_start_time").val("12:00am");
    $("#deal_deal_end_time").val("11:59pm");

  });

  $("#limited").change(function(){
    if(!$("#deal_limited").is(":checked")){
      toggle_limited_unchecked();

      $('#deal_original_qty').val(0);
    }
    else{
      toggle_limited_checked();
    }
  });

  $('input[name^="deal[branches]"]').change(function(){
    $(this).is(':checked') ? $(this).val(1) : $(this).val(0)
  })

  //set initial state.
  $('#deal_all_branches').val(1);

  $('#deal_all_branches').change(function() {
    if(!$(this).is(":checked")) {
      toggle_all_branches_unchecked();
    }
    else {
      toggle_all_branches_checked();

      $('#count_box').show();
    }
    $('#deal_all_branches').val($(this).is(':checked'));
  });

  click_next();

  // $("#deal_redeem_steps").wysihtml5({'toolbar': {'blockquote': false, 'html': true,'image' : false}});

  $("#deal_terms_and_conditions, #deal_additional_info").froalaEditor(
    {
      height: 300,
      requestWithCORS: false,
      toolbarButtons: ['paragraphFormat', '|', 'bold', 'italic', 'underline', '|', 'align', '|', 'outdent', 'indent', '|', 'formatUL', 'formatOL', '|', 'insertImage', '|', 'insertLink', 'insertVideo', '|', 'html'],
      imageUploadURL: "https://api.cloudinary.com/v1_1/getpickledeals/auto/upload",
      imageUploadParams: {
          upload_preset: "ovkrdjbz",
          api_key: "553722124845437"
      }
    });

  $('a[href="https://froala.com/wysiwyg-editor"]').parent().remove();
});

var _parseJSON = jQuery.parseJSON;
jQuery.parseJSON = function(j) {
    var response = _parseJSON(j);
    response.link = response.url;
    return response;
};

function initAutoComplete(){
  $('.auto_search_complete').autocomplete({
    minLength: 3,
    source: function(request, response) {
      $.ajax({
        url: "/businesses/autocomplete",
        dataType: "json",
        data: {q: request.term},
        success: function( data ) {
          response( data );
        }
      });
    },
    select: function( event, ui ) {
      choose_autocomplete(ui,"new");
      populate_prefill_text(ui);
      toggle_all_branches_unchecked(ui.item.label);
    },
    focus: function( event, ui ) {
      $( "#auto_search_complete" ).val( ui.item.label );
      return false;
    },
  }).autocomplete( "instance" )._renderItem = function( ul, item ) {
    return $( "<li>" )
    .append( "<a>" + item.id + " : " +  item.label + "</a>" )
    .appendTo( ul );
  };
}

function click_next(){
  $('.js-wizard-simple').bootstrapWizard({
    'onNext': function(tab, navigation, index) {
      return false;
    }});

    // $('.wizard-next').click(function(e){
    //   var form = $(this).parents("form");
    //   if ((form.find("input#deal_name") === "" ) || (form.find("input#deal_price") === "") || (form.find("input#deal_deal_price") === "") || (form.find("textarea#description") === "") || (form.find("input#deal_deal_start_time") === "") || (form.find("input#deal_deal_end_time") === "") || (form.find("input#deal_start_date") === "") || (form.find("input#deal_end_date") === "")){
    //     return true;
    //   }
    //   else{
    //     // alert(123);
    //     // $(".js-wizard-simple").bootstrapWizard('next');
    //   }
    // });
    // var $validator = $("#deal_form").validate({
    //   rules: {
    //     deal_name: {
    //       required: true,
    //       minlength: 3
    //     }
    //   }
    // });
    //
    // $('#rootwizard').bootstrapWizard({
    //   'tabClass': 'nav nav-tabs',
    //   'onNext': function(tab, navigation, index) {
    //     alert(123);
    //     var $valid = $("#deal_form").valid();
    //     if(!$valid) {
    //       $validator.focusInvalid();
    //       return false;
    //     }
    //   }
    // });
}
