/*
 *  Document   : base_forms_wizard.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in Form Wizard Page
 */
function validForm(tab,tag){
  var tab_id = tab.find("a").attr("href");
  var part_form = $("#deal_form " + tab_id);
  var isValid = true;
  tag = tag.split(',').join(':visible:enabled,') + ':visible:enabled';
  part_form.find(tag).each(function() {
    var element = $(this);
    if (element.val() == "" || element.val() == null) {
      isValid = false;
    }
  });
  // if (parseInt($('#deal_price').val()) == 0) {
  //   notys("Price can't be 0","error");
  // }
  // if (parseInt($('#deal_deal_price').val()) == 0) {
  //   notys("Offer price can't be 0","error");
  // }
  if (tab_id == '#tab-branch') {
    var checkSelection = $('.branch-deal-check:checked').length + $('.redeem-type-selection:checked').length;
    if (checkSelection == 0) {
      isValid = false;
    }
  }
  return isValid;
}

function saveDeal(){
  tab = $("ul.nav-tabs li.active");
  var isValid = true;
  var addlInfo = $("#deal_additional_info").froalaEditor('html.get');
  var t_and_c = $("#deal_terms_and_conditions").froalaEditor('html.get');
  if (t_and_c == "") {
    isValid = false;
  }
  if (!isValid){
    notys("Fill all fields of the form to proceed!","error");
    return false;
  }

  var dealForm = $('#deal_form');
  var formData = dealForm.serialize();
  $.ajax({
      type: "POST",
      url: formData.attr('action'),
      data: formData,
      dataType: "JSON",
  }).success(function(json){
    window.location.href = '/deals';
  }).error(function(response) {
    if (response.status == 400) {
      $.each(response.responseJSON.response, function(k, v) {
        notys(v,"error");
      });
    } else if (response.status >= 500) {
      notys("Something went wrong","error");
    }
  });
  return false;
}

var BaseFormWizard = function() {
  // Init simple wizard, for more examples you can check out http://vadimg.com/twitter-bootstrap-wizard-example/
  var initWizardSimple = function(){
    jQuery('.js-wizard-simple').bootstrapWizard({
      'tabClass': '',
      'onNext': function(tab, navigation, index) {

        var dealPrice = $('#deal_price:visible:enabled');
        var dealDealPrice = $('#deal_deal_price:visible:enabled');
        if (dealPrice.length > 0 && dealDealPrice.length > 0) {
          if (parseInt(dealPrice.val()) == 0) {
            notys("Price can't be zero","error");
            return false;
          }
          if (parseInt(dealDealPrice.val()) == 0) {
            notys("Offer price can't be zero","error");
            return false;
          }
        }

        if (validForm(tab,"input[type='text'],select")){
          return true;
        }
        else {
          notys("Fill all fields of the form to proceed!","error");
          return false;
        }
      },
      'firstSelector': '.wizard-first',
      'previousSelector': '.wizard-prev',
      'nextSelector': '.wizard-next',
      'lastSelector': '.wizard-last',
      'onTabShow': function($tab, $navigation, $index) {
        var $total      = $navigation.find('li').length;
        var $current    = $index + 1;
        var $percent    = ($current/$total) * 100;

        // Get vital wizard elements
        var $wizard     = $navigation.parents('.block');
        var $progress   = $wizard.find('.wizard-progress > .progress-bar');
        var $btnPrev    = $wizard.find('.wizard-prev');
        var $btnNext    = $wizard.find('.wizard-next');
        var $btnFinish  = $wizard.find('.wizard-finish');

        // Update progress bar if there is one
        if ($progress) {
          $progress.css({ width: $percent + '%' });
        }

        // If it's the last tab then hide the last button and show the finish instead
        if($current >= $total) {
          $btnNext.hide();
          $btnFinish.show();
        } else {
          $btnNext.show();
          $btnFinish.hide();
        }
      }
    });
  };

  // Init wizards with validation, for more examples you can check out http://vadimg.com/twitter-bootstrap-wizard-example/
  var initWizardValidation = function(){
    // Get forms
    var $form1 = jQuery('.js-validation-bootstrap');
    var $form2 = jQuery('.js-form2');

    // Prevent forms from submitting on enter key press
    $form1.add($form2).on('keyup keypress', function (e) {
      var code = e.keyCode || e.which;

      if (code === 13) {
        e.preventDefault();
        return false;
      }
    });

    // Init form validation on classic wizard form
    var $validator1 = $form1.validate({
      errorClass: 'help-block animated fadeInDown',
      errorElement: 'div',
      errorPlacement: function(error, e) {
        jQuery(e).parents('.form-group > div').append(error);
      },
      highlight: function(e) {
        jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
        jQuery(e).closest('.help-block').remove();
      },
      success: function(e) {
        jQuery(e).closest('.form-group').removeClass('has-error');
        jQuery(e).closest('.help-block').remove();
      },
      rules: {
        'deal_name': {
          required: true,
          minlength: 2
        },
        'validation-classic-lastname': {
          required: true,
          minlength: 2
        },
        'validation-classic-email': {
          required: true,
          email: true
        },
        'validation-classic-details': {
          required: true,
          minlength: 5
        },
        'validation-classic-city': {
          required: true
        },
        'validation-classic-skills': {
          required: true
        },
        'validation-classic-terms': {
          required: true
        }
      },
      messages: {
        'deal_name': {
          required: 'Please enter a firstname',
          minlength: 'Your firtname must consist of at least 2 characters'
        },
        'validation-classic-lastname': {
          required: 'Please enter a lastname',
          minlength: 'Your lastname must consist of at least 2 characters'
        },
        'validation-classic-email': 'Please enter a valid email address',
        'validation-classic-details': 'Let us know a few thing about yourself',
        'validation-classic-skills': 'Please select a skill!',
        'validation-classic-terms': 'You must agree to the service terms!'
      }
    });

    // Init form validation on the other wizard form
    var $validator2 = $form2.validate({
      errorClass: 'help-block text-right animated fadeInDown',
      errorElement: 'div',
      errorPlacement: function(error, e) {
        jQuery(e).parents('.form-group > div').append(error);
      },
      highlight: function(e) {
        jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
        jQuery(e).closest('.help-block').remove();
      },
      success: function(e) {
        jQuery(e).closest('.form-group').removeClass('has-error');
        jQuery(e).closest('.help-block').remove();
      },
      rules: {
        'validation-firstname': {
          required: true,
          minlength: 2
        },
        'validation-lastname': {
          required: true,
          minlength: 2
        },
        'validation-email': {
          required: true,
          email: true
        },
        'validation-details': {
          required: true,
          minlength: 5
        },
        'validation-city': {
          required: true
        },
        'validation-skills': {
          required: true
        },
        'validation-terms': {
          required: true
        }
      },
      messages: {
        'validation-firstname': {
          required: 'Please enter a firstname',
          minlength: 'Your firtname must consist of at least 2 characters'
        },
        'validation-lastname': {
          required: 'Please enter a lastname',
          minlength: 'Your lastname must consist of at least 2 characters'
        },
        'validation-email': 'Please enter a valid email address',
        'validation-details': 'Let us know a few thing about yourself',
        'validation-skills': 'Please select a skill!',
        'validation-terms': 'You must agree to the service terms!'
      }
    });

    // Init classic wizard with validation
    jQuery('.js-wizard-classic-validation').bootstrapWizard({
      'tabClass': '',
      'previousSelector': '.wizard-prev',
      'nextSelector': '.wizard-next',
      'onTabShow': function($tab, $nav, $index) {
        var $total      = $nav.find('li').length;
        var $current    = $index + 1;

        // Get vital wizard elements
        var $wizard     = $nav.parents('.block');
        var $btnNext    = $wizard.find('.wizard-next');
        var $btnFinish  = $wizard.find('.wizard-finish');

        // If it's the last tab then hide the last button and show the finish instead
        if($current >= $total) {
          $btnNext.hide();
          $btnFinish.show();
        } else {
          $btnNext.show();
          $btnFinish.hide();
        }
      },
      'onNext': function($tab, $navigation, $index) {
        var $valid = $form1.valid();

        if(!$valid) {
          $validator1.focusInvalid();

          return false;
        }
      },
      onTabClick: function($tab, $navigation, $index) {
        return false;
      }
    });

    // Init wizard with validation
    jQuery('.js-wizard-validation').bootstrapWizard({
      'tabClass': '',
      'previousSelector': '.wizard-prev',
      'nextSelector': '.wizard-next',
      'onTabShow': function($tab, $nav, $index) {
        var $total      = $nav.find('li').length;
        var $current    = $index + 1;

        // Get vital wizard elements
        var $wizard     = $nav.parents('.block');
        var $btnNext    = $wizard.find('.wizard-next');
        var $btnFinish  = $wizard.find('.wizard-finish');

        // If it's the last tab then hide the last button and show the finish instead
        if($current >= $total) {
          $btnNext.hide();
          $btnFinish.show();
        } else {
          $btnNext.show();
          $btnFinish.hide();
        }
      },
      'onNext': function($tab, $navigation, $index) {
        var $valid = $form2.valid();

        if(!$valid) {
          $validator2.focusInvalid();

          return false;
        }
      },
      onTabClick: function($tab, $navigation, $index) {
        return false;
      }
    });
  };

  return {
    init: function () {
      // Init simple wizard
      initWizardSimple();
      //            initWizardValidation();

    }
  };
}();

// Initialize when page loads
jQuery(function(){ BaseFormWizard.init(); });
