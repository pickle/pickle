// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery.min
//= require jquery_ujs
//= require moment.min
//= require jquery_nested_form
//= require cloudinary
//= require jquery.noty.packaged.min
//= require bootstrap-noty
//= require jquery-ui.min
//= require jquery-ui
//= require_tree .
//= require_tree ./plugins
//= require bootstrap-wysihtml5
//= require froala_editor.min.js
//= require plugins/paragraph_format.min.js
//= require plugins/align.min.js
//= require plugins/lists.min.js
//= require plugins/link.min.js
//= require plugins/image.min.js
//= require plugins/code_view.min.js
//= require plugins/video.min.js


function check_and_remove_add(f,what,limit){
  var limit = limit;
  var actual = $(what + " .fields").length;
  if (limit == actual){
    $("a.add_image_link").remove();
  }
}

function image_add_cb(div_cl){
  $(div_cl).focusout(function(){
    var url = $(this).val();
    var f_group = $(this).closest(".form-group");
    var prev_div = f_group.siblings(".form-group").find(".image_preview");
    var medium = f_group.siblings("input.medium");
    var thumb = f_group.siblings("input.thumb");
    var m_url = url.replace("upload\/","upload/w_560,c_fit/");
    var t_url = url.replace("upload\/","upload/w_72,c_thumb/");

    prev_div.html("<img height=150 src=" + url + ">");
    medium.val(m_url);
    thumb.val(t_url);
  });
}


function actmm(what){
  $('ul.nav-main li a').removeClass('active');
  $('a#' + what).addClass('active');
}

function notys(txt,type){
  var n = noty({
    layout: 'bottomLeft',
    template: '<div class="noty_message"><span class="noty_text"></span><div class="noty_close"></div></div>',
    type: type,
    theme: 'bootstrapTheme',
    text: txt
  });
}


$(function(){

    // this catches enter key and returns false
  $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });

  $(".js-datepicker").datepicker();

  var nowDate = new Date();
  var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
  $(".js-datepicker-future").datepicker({startDate: today});

  $(".specific_branches_radio").click(function(){
    var all_branches = $(this).val();
    var business_id = $("#deal_business_id").val();
    var type = $("div#deals_form").data("type");
    url = '/businesses/' + business_id + '/branches';

    if (all_branches == "false" && type == "new"){
      $.getScript(url);
      goToByScroll("specific_branches");

    }
    else if(all_branches == "false" && type == "edit"){
      var deal_id = $("div#deals_form").data("deal_id");
      url = url + "?deal_id=" + deal_id
      $.getScript(url);
    }
  });

  $(".all_branches_radio").click(function(){
    $("#specific_branches").empty();
  });


});
function home_charts(){
  $.get('/charts_by_week',function(json){
    var chart = new Highcharts.Chart({
      title: {
        text: "Deals - That Expires Around This Week"
      },
      chart: {
        type: 'area',
        renderTo: "chart_container"
      },
      xAxis: {
        title: {
          text: "Today - 5 days And Today + 7 Days"
        },
        categories: json["cats"]
      },
      yAxis: {
        title: {
          text: "Number Of Deals"
        }
      },
      plotOptions: {
        series: {
          allowPointSelect: true
        }
      },
      series: [{
        name: "Deals Count",
        data: json["counts"]
      }]
    });
  });
}

function goToByScroll(id){
  // Remove "link" from the ID
  id = id.replace("link", "");
  // Scroll
  $('html,body').animate({
    scrollTop: $("#"+id).offset().top},
    'slow');
}
