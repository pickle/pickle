class CitiesController < ApplicationController

  before_action :set_city, only: [:show, :edit, :update, :destroy]

  def index
    authorize! :list, City
    @cities = City.page(params[:page])
  end

  def new
    authorize! :create, City
    @city = City.new
  end

  def create
    if city = write("post","api/cities",{'city' => city_params})
      city_id = city["response"]["city"]["id"]
      redirect_to cities_path
    else
      redirect_to new_city_path
    end
  end

  def edit
    authorize! :update, City
  end

  def update
    if city = write("put","api/cities/#{@city.id}",{'city' => city_params})
      city_id = city["response"]["city"]["id"]
      redirect_to cities_path
    else
      redirect_to edit_city_path(@city.id)
    end
  end

  def destroy
    authorize! :delete, City
    city = write("delete","api/cities/#{@city.id}")
    if city["response"]["message"] == "Deleted Successfully!"
      redirect_to cities_path
    else
      redirect_to cities_path
    end
  end

  private

  def set_city
    @city = City.find_by(id: params[:id])
  end

  def city_params
    params.require(:city).permit(:name, :country_id)
  end
end
