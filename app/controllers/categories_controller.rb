class CategoriesController < ApplicationController

  load_and_authorize_resource

  before_action :set_category, only: [:show, :edit, :update, :destory]

  def index
    authorize! :list, Category
    @categories = Category.page(params[:page])
  end

  def new
    authorize! :create, Category
    @category = Category.new
  end

  def create
    if category = write("post","api/categories",{'category' => category_params})
      category_id = category["response"]["category"].present? ? category["response"]["category"]["id"] : category["response"]["sub_category"]["id"]
      redirect_to categories_path
    else
      redirect_to new_category_path
    end
  end

  def edit
    authorize! :update, Category
  end

  def update
    if category = write("put","api/categories/#{@category.id}",{'category' => category_params})
      category_id = category["response"]["category"].present? ? category["response"]["category"]["id"] : category["response"]["category"]["id"]
      redirect_to categories_path
    else
      redirect_to edit_category_path(@category.id)
    end
  end


  def show
  end

  def destroy

    authorize! :delete, Category
    category = write("delete","api/categories/#{@category.id}")
    if category["response"]["message"] == "Deleted Successfully!"
      redirect_to categories_path
    else
      redirect_to categories_path
    end
  end

private

  def set_category
    @category = Category.find_by(id: params[:id])    
  end

  def category_params
    params.require(:category).permit(:name, :description, :keywords, :parent_id, :image)
  end
end
