class HomeController < ApplicationController

  def dashboard
    @hsh = {}
    @hsh[:businesses_with_deals], @hsh[:businesses_without_deals] = dealless_businesses
    @hsh[:businesses_with_highbranches], @hsh[:business_names] = highbranch_businesses
    @hsh[:deals],@hsh[:favs] = deals_favourites
    @hsh[:orphaned_categories],@hsh[:category_names] = orphaned_categories
  end

  def dealless_businesses
    deals = (Deal.select("DISTINCT(business_id)").map &:business_id).count
    businesses = (Business.select("DISTINCT(id)").map &:id).count
    [deals,(businesses - deals)]
  end

  def highbranch_businesses(i = 1)
    out = Business.joins(:branches).group("businesses.id").having("count(branches.id) > ?",i).count
    few = out.take(2)
    businesses = Business.where(id: (few.map &:first)).map &:name || ["",""]
    txt = [businesses[0],"(",few.first[1],")",","," ",businesses[1],"(",few.last[1],")"].join rescue "Weird"
    [out.count,txt]
  end

  def deals_favourites
    deals = Deal.all.count
    favs = DealFavorite.select("DISTINCT(deal_id)").count
    [deals,favs]
  end

  def orphaned_categories
    deals = Deal.select("DISTINCT(sub_category_id) as ids").map &:ids
    business = Business.select("DISTINCT(category_id) as ids").map &:ids
    categories = Category.select("id").map &:id
    orphans = categories.compact - (deals.compact + business.compact)
    names = Category.where(id: orphans.take(3)).map{|i| i.name[0..9] }
    [orphans.count,names.join(",")]
  end

  def charts_by_week
    out = Deal.where("end_date between '#{(Time.now.to_date - 5).to_date}' and '#{Time.now.to_date + 7}'").group("end_date").count
    json = {cats: out.keys, counts: out.values}
    render json: json.to_json
  end
end
