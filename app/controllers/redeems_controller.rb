
class RedeemsController < ApplicationController
  def index
  end

  def code_enter
    find_order
    if !@msg.blank?
      flash.now[:error] = @msg
    end
    render "redeems/index"
  end

  def redeem_code
    find_order
    process_order
    if @process_success
      find_order
      flash[:notice] = "The deal has been successfully redeemed"
      render "redeems/index"
  else
      flash[:error] = @msg
      render "redeems/index"
    end
  end

  def find_order
    if params[:deal_code].blank?
      @msg = "Please enter a deal code"
      return
    end

    @type = 'regular'
    @is_punched_deal = false

    @resp = write("get","api/find_by_code/#{params[:deal_code]}",{})
    @msg = ""

    if @resp["response"].nil? or @resp["response"]["deals"].nil? or @resp["response"]["deals"].size == 0
      @punch_card = PunchedCardBusiness.where(stamp_code: params[:deal_code]).first
      if @punch_card
        PunchedCardBusiness.uncached do
          @stamped_count = PunchedCardBusiness
            .joins(:punched_card_users)
            .where('punched_card_businesses.deal_id = ? AND punched_card_businesses.created_by = ? AND punched_card_users.used = 0', @punch_card.deal_id, @punch_card.created_by)
            .count
        end
        @type = 'punched_card'
      else
        @msg = "That deal code is invalid"
        return
      end
    end

    @deal_id = @type == 'regular' ? @resp['response']['deals'].first['id'] : @punch_card['deal_id']
    @deal_info_resp = write('get', "api/deal_info/#{@deal_id}", {})
    @deal_info = @deal_info_resp['response']['deal_info']

    begin
      if @type == 'regular'
        @deal = @resp["response"]["deals"].first
        @order = @deal["orders"].first
        @user = User.find(@order["bought_by"])
        @buy_type = @order["buy_type"]
        if @deal["deal_type"] == "punched_cards"
          @is_punched_deal = true
        end
      elsif @type == 'punched_card'
        @deal = @deal_info
        @user = User.find(@punch_card.created_by)
        @buy_type = 'punch_card'
      end

      @business = @deal['business']

      # Validate if the deal belongs to this business
      if current_admin.role != 'admin'
        is_code_from_business = current_admin.businesses.where(id: @business['id']).exists?
        if !is_code_from_business
          @msg = "Sorry, this code does not belong to your business"
          @deal = nil
          return
        end
      end

      @available_days = @deal["available_days_cons"].join(', ')
      @branch = Branch.find(@deal["branches_deals"].first["branch"]['id'])
      @image = @deal["images"].first
      if @user
        @user_redeemed_deals = BranchesDeal.joins(:deal_codes).where('branch_id = ? AND (deal_codes.bought_by = ? AND deal_codes.redeemed_at IS NOT NULL)', @branch.id, @user.id).count()
        @user_total_deal_spend = BranchesDeal.joins(:deal_codes, :deal).where('deal_codes.bought_by = ? AND deal_codes.redeemed_by IS NOT NULL AND branches_deals.branch_id = ?', @user.id, @branch.id).sum('deals.deal_price')
        @user_total_deal_spend = @user_total_deal_spend.round(2)
        @user_total_stamps_punched = PunchedCardUser.where(user_id: @user.id, used: true).count()
      end
      @business_name = @deal["business"]["name"]
      @business_logo = @deal["business"]["logo"]
      @user_history = BranchesDeal.joins(:deal_codes).where("branch_id = ? AND (deal_codes.bought_by = ? AND deal_codes.redeemed_at IS NOT NULL)", @branch.id, @user.id).order('deal_codes.redeemed_at')

      set_use_between_text
      set_pay_msg

      # Set redeem_info as 'v' if this is a punched_card deal
      redeem_info = ''
      if @type == 'regular'
        redeem_info = @deal['redeem_info']
      elsif @type == 'punched_card'
        PunchedCardUser.uncached do
          if PunchedCardUser.where(stamp_id: @punch_card.id).exists?
            redeem_info = 'u'
          else
            redeem_info = 'v'
          end
        end
      end

      @is_redeemable = redeem_info == 'v'
      @deal_status = "Valid"

      if !@is_redeemable
        if redeem_info == 'dt'
          @deal_status = "Refer Date/Time"
        elsif redeem_info == 'e'
          @deal_status = "Code Expired"
        elsif redeem_info == 'u'
          @deal_status = 'Already Used'
        end
      end

    rescue => e
      Rails.logger.info "================= Deal Load Error ================="
      Rails.logger.info e.message
      Rails.logger.info e.backtrace.join('\n')
      @msg = "Something went wrong in finding order"
    end
  end

  def process_order
    @process_success = false
    if @type == 'regular'
      if @order["code_status"] == "unredeemed"
        actual_redeem
      elsif (@order["code_status"] == "redeemed")
        @msg = "Already Used"
      else
        @msg = "Order found and something went wrong"
      end
    elsif @type == 'punched_card'
      actual_redeem_punch_card
    end
  end

  def set_use_between_text
    @use_between_text = ''
    case @deal_info['redeem_type']
    when 'time'
      @use_between_text = @deal_info["start_time"] + " - " + @deal_info["end_time"]
    when 'date'
      @use_between_text = @deal_info["start_date"] + ' - ' + @deal_info["end_date"]
    when 'description'
      @use_between_text = "See description"
    when 'working_hours'
      @use_between_text = "Working hours"
    end
  end

  def set_pay_msg
    if @buy_type == "cash"
      @pay_msg = "PAY AT STORE"
    elsif @buy_type == "online"
      @pay_msg = "PAID IN FULL"
    elsif @buy_type == "punch_card"
      @pay_msg = "NA"
    end
  end

  def actual_redeem
    redeem_options = {}
    redeem_options[:branch_id] = @order["branch_id"]
    redeem_options[:deal_code] = @order["code"]
    redeem_resp = write("post","api/deals/#{@deal['id']}/redeem",redeem_options)
    %w(success error).each do |i|
      temp = redeem_resp["response"][i]
      @msg = temp if !temp.nil?
    end
    @process_success = true if redeem_resp["response"].key?('success')
  end

  def actual_redeem_punch_card
    redeem_options = {}
    if @stamped_count == @punch_card.deal.no_of_stamps - 1
      redeem_options[:deal] = {
        :branch_id => @branch.id
      }
    else
      redeem_options[:deal] = {
        :stamp_code => @punch_card.stamp_code
      }
    end
    redeem_resp = write("post","api/deals/#{@deal['id']}/stamp",redeem_options)
    @process_success = true if redeem_resp["response"].key?('punched_cards')
    if @process_success
      @msg = 'Stamp code has been punched'
    else
      @msg = redeem_resp["response"]["stamp"]
    end
  end

end
