class SpecialsController < ApplicationController
  before_action :set_special, only: [:show, :edit, :update, :destroy]

  def get_branch_ids
    branches = Special.find(params[:id]).branches.pluck :id
    render json: branches
  end
  # GET /specials
  # GET /specials.json
  def index
    @specials = Special.includes(:business).order("created_at DESC").page(params[:page]).per(params[:limit] || 10)
  end

  # GET /specials/1
  # GET /specials/1.json
  def show
  end

  # GET /specials/new
  def new
    @special = Special.new
  end

  def create_or_update(method,url)
    path = "api/specials"
    path = path + "/#{@special.id}" if method == "put"
    sp = special_params
    sp[:all_branches] = bool(sp[:all_branches])
    sp[:branches_specials].map! &:to_i if sp[:branches_specials]
    special = write(method,path, {'special' => sp})

    if special['response']['special']
      redirect_to specials_path
    else
      redirect_to send("#{url}_special_path"), notice: special['response']
    end
  end

  # GET /specials/1/edit
  def edit
  end

  # POST /specials
  # POST /specials.json
  def create
    create_or_update("post","new")
  end
  def update
    create_or_update("put","edit")
  end

  # DELETE /specials/1
  # DELETE /specials/1.json
  def destroy
    @special.destroy
    respond_to do |format|
      format.html { redirect_to specials_url, notice: 'Special was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_special
    @special = Special.find(params[:id])
  end

  def special_params
    params.require(:special).permit!
  end
end
