class CountriesController < ApplicationController

  before_action :set_country, only: [:show, :edit, :update, :destroy]

  def index
    authorize! :list,Country
    @countries = Country.page(params[:page])
  end

  def new
    authorize! :create,Country
    @country = Country.new
  end

  def create
    if country = write("post","api/countries",{'country' => country_params})
      country_id = country["response"]["country"]["id"]
      redirect_to countries_path
    else
      redirect_to new_country_path
    end
  end

  def edit
    authorize! :update,Country
  end

  def update
    if country = write("put","api/countries/#{@country.id}",{'country' => country_params})
      country_id = country["response"]["country"]["id"]
      redirect_to countries_path
    else
      redirect_to edit_country_path(@country.id)
    end
  end

  def destroy
    authorize! :delete,Country
    country = write("delete","api/countries/#{@country.id}")
    if country["response"]["message"] == "Deleted Successfully!"
      redirect_to countries_path
    else
      redirect_to countries_path
    end
  end

  private

  def set_country
    @country = Country.find_by(id: params[:id])
  end

  def country_params
    params.require(:country).permit(:name)
  end
end
