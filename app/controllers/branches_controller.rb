class BranchesController < ApplicationController

  before_action :set_business, only: [:new, :create, :edit, :update, :index]
  before_action :set_branch, only: [:show, :edit, :update, :destroy]

  def index
    @q = @business.branches.order("created_at DESC").ransack(params[:q])
    @branches = @q.result.includes(:area).page(params[:page]).per(params[:limit] || 20)
    respond_to do |format|
      format.html
      format.json {render json: result}
    end
  end

  def new
    authorize! :create, Branch
    @branch = @business.branches.new
  end

  def show
    authorize! :read, Branch
  end

  def create
    create_or_update("post","new")
  end

  def update
    create_or_update("put","edit")
  end

  def create_or_update(method,url)
    path = "api/branches"
    path = path + "/#{@branch.id}" if method == "put"
    branch = branch_params
    branch[:business_id] = params[:business_id]
    branch = format_image(branch) if branch["images_attributes"].present?
    branch = format_attachments(branch) if branch["attachments_attributes"].present?
    branch = process_params(branch)
    puts branch
    resp = write(method,path,{'branch' => branch })
    puts resp
    if resp["response"]["branch"].present?
      # branch_id = branch["response"]["branch"]["id"]
      redirect_to business_branches_path #(branch_id)
    else
      res = resp["response"]
      out = res.map{|k,v| ("#{k} - #{v.join(",")}" rescue nil)}.join(',')
      flash[:error] = out

      render :new
    end
  end

  def process_params(b)
    b = process_timeslots(b)
    b = process_weekdays(b)
  end

  def process_timeslots(b)
    b[:time_slots] = Hash.new()
    %w(sun mon tue wed thu fri sat).each do |i|
      b[:time_slots][i] = Array.new()
      start_time = b[:timing][i][0]
      end_time = b[:timing][i][1]
      start_time = time_to_seconds(start_time) if start_time.present?
      end_time = time_to_seconds(end_time) if end_time.present?
      b[:time_slots][i][0] = start_time.to_s + "-" + end_time.to_s
      if b[:timing][i].length == 4
        start_time_2 = time_to_seconds(b[:timing][i][2])
        end_time_2 = time_to_seconds(b[:timing][i][3])
        b[:time_slots][i][1] = start_time_2.to_s + "-" + end_time_2.to_s
      end
    end
    b.delete(:timing)
    b
  end

  def process_weekdays(b)
    b[:working_days] = Array.new()
    weekday_mapping = {
      sun: "Sunday", mon: "Monday", tue: "Tuesday", wed: "Wednesday", thu: "Thursday", fri: "Friday", sat: "Saturday"
    }
    weekday_mapping.each do |key, day|
      if b[key] == "1"
        b[:working_days] << day
      end
      b.delete(key)
    end
    b
  end

  def edit
    authorize! :update, Branch
  end

  def destroy
    authorize! :delete, Branch
    resp = write("delete","api/branches/#{@branch.id}",{})
    redirect_to businesses_path
  end

  def branches_deals
    hsh = {}
    Business.find_by(name: params[:q]).branches.map {|i| hsh[i.id] = [i.id,i.name,0,i.area.try(:name)] }
    branches_deals = Deal.find(params[:deal_id]).branches_deals

    if !((branches_deals.map &:branch_id) & hsh.keys).blank?
      branches_deals.map {|i| hsh[i.branch_id][2] = (hsh[i.branch_id].present? ? i.original_qty : "false") }
    end
    render json: hsh.values
  end

  def search_by_business_name
    business = Business.find_by(name: params[:q])
    branches = business.branches
    area_ids = branches.pluck(:area_id)
    areas = Area.where(id: area_ids).select(:id,:name).as_json
    hsh = {}
    areas.map {|i| hsh[i["id"]] = i["name"]}
    branch_data = branches.pluck(:id, :name, :area_id)
    branch_data = branch_data.map{|arr| [arr[0],arr[1],hsh[arr[2]]] }
    result = {
      business_terms: business.terms_and_conditions,
      branches: branch_data
    }
    render json: result
  end

  private

  def branch_params
    params.require(:branch).permit!#(:name, :working_days, :open_time, :close_time, :address, :branch_area, :landline, :additional_info, :location_id, :business_id, :state, :latitude, :longitude, :sun, :mon, :tue, :wed, :thu, :fri, :sat)
  end

  def set_business
    @business = Business.find_by(id: params[:business_id])
  end

  def set_branch
    @branch = Branch.find_by(id: params[:id])
  end

end
