require 'time'

class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  include ApiCall
  include ImageUploader
  protect_from_forgery with: :exception
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to authenticated_root_url, :alert => exception.message
  end

  before_action :authenticate_admin!
  layout :layout_by_resource

  # https://github.com/ryanb/cancan/issues/656
  # Overwritten current_ability class from CanCanCan to get abiltiy of model other than users
  def current_ability
    Ability.new(current_admin)
  end

  def bool(i)
    if i == "true"
      out = true
    elsif i.to_i == 1
      out = true
    else
      out = false
    end
    out
  end

  protected

  def layout_by_resource
    if devise_controller?
      "login"
    else
      "application"
    end
  end

  def format_image(resource)
    images = Array.new
    p resource
    resource["images_attributes"].map{|k, v|
      hsh = v.delete_if{|i,j| j.blank? }
      images << hsh.slice(:original,:medium,:thumb) if (!hsh.blank? && (hsh["_destroy"] == "false"))
    }
    p 'images'
    p images
    resource.delete("images_attributes")
    resource.merge!("images" => images)
  end

  def format_attachments(resource)
    attachments = Array.new
    resource["attachments_attributes"].map{|key, attachment_data|
      hsh = attachment_data.delete_if{|i,j| j.blank? }
      if attachment_data['images_attributes']
        images = Array.new
        attachment_data['images_attributes'].map{|key, image_data|
          img_hsh = image_data.delete_if{|i,j| j.blank? }
          p img_hsh
          images << img_hsh.slice(:image) if (!img_hsh.blank? && (img_hsh["_destroy"] == "false"))
          p images
        }
        attachments << {title: hsh[:title], images_attributes: images} if (!hsh.blank? && (hsh["_destroy"] == "false") && images.length > 0)
      end
    }
    resource.delete("attachments_attributes")
    resource.merge!("attachments" => attachments)
  end

  def time_to_seconds(i)
    Time.parse(i).seconds_since_midnight
  end

  def seconds_to_time(t)
    Time.at(t).utc.strftime("%I:%M%P")
  end

end
