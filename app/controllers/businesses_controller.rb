class BusinessesController < ApplicationController

  # load_and_authorize_resource

  before_action :set_business, only: [:show, :update, :edit, :approve, :disapprove, :destroy]

  def search_name
    businesses = Business.where("name like '#{params[:q]}%'").select("id,concat(id,'-',name) as name")
    respond_to do |format|
      format.json { render :json => businesses.map(&:attributes) }
    end
  end

  def get_prefill_text
    @business = Business.find(params[:id].to_i)
    render json: @business
  end

  def search_keywords
    @keywords = Keyword.where("name like ?", "#{params[:q]}%")
    respond_to do |format|
      format.json { render :json => @keywords.map(&:attributes) }
    end
  end

  def add_keyword
    resp = write("post","api/keywords",{keyword: {name: params[:keyword]}})
  end

  def index
    authorize! :list, Business
    @q = fetch_businesses.order("created_at DESC").ransack(params[:q])
    @businesses = @q.result.includes(:category).page(params[:page]).per(params[:limit] || 20)
    @branches_count = Branch.joins(:business).where("businesses.id IN (#{(@businesses.map &:id).join(",")})").group(:business_id).count if !@businesses.blank?
  end

  def fetch_businesses
    if current_admin.admin?
      Business
    else
      current_admin.businesses
    end
  end

  def new
    print 'test'
    authorize! :create, Business
    @business = Business.new
    1.times{ @business.images.build}
  end

  def create
    create_or_update("post","new")
  end

  def update
    create_or_update("put","edit")
  end

  def edit
    authorize! :update, Business
    @images = @business.images
  end

  def create_or_update(method,url)
    path = "api/businesses"
    path = path + "/#{@business.id}" if method == "put"
    business = business_params
    business = format_image(business) if business["images_attributes"].present?
    business = format_keywords(business) if business[:add_keywords].present?
    resp = write(method,path,{'business' => business })
    if resp["response"]["business"].present?
      # business_id = business["response"]["business"]["id"]
      redirect_to businesses_path #(business_id)
    else
      res = resp["response"]
      out = res.map{|k,v| "#{k} - #{v.join(",")}"}.join(',')
      flash[:error] = out
      redirect_to send("#{url}_business_path")
    end
  end

  def show

  end

  def destroy
    authorize! :delete, Business
    business = write("delete","api/businesses/#{@business.id}")
    if business["response"]["message"] == "Deleted Successfully!"
      redirect_to businesses_path
    else
      redirect_to businesses_path
    end
  end

  def autocomplete
    result = Array.new
    res = Business.autocomplete(params[:q])
    render json: res.map { |r|  {"id" => r.id,"label" => "#{r.name}"}} if res.present?
    #render json: res.map(&:name)
  end

  private

  def format_keywords(p)
    keywords = p[:add_keywords]
    arr = []
    keywords.split(",").each do |i|
      arr << {"keyword_id" => i}
    end
    p[:keywords] = arr
    p.delete(:add_keywords)
    p
  end

  def set_business
    @business = Business.find(params[:id])
  end

  def business_params
    params.require(:business).permit!#(:id,:_destroy,:name, :category_id, :description, :website, :facebook, :keywords, :twitter, :logo, :state, images_attributes: [:original, :medium, :thumb])
  end

end
