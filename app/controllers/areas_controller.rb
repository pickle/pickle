class AreasController < ApplicationController

  before_action :set_area, only: [:show, :edit, :update, :destroy]

  def index
    authorize! :list, Area
    @areas = Area.page(params[:page])
  end

  def new
    authorize! :create, Area
    @area = Area.new
  end

  def create
    if area = write("post","api/areas",{'area' => area_params})
      area_id = area["response"]["area"]["id"]
      redirect_to areas_path
    else
      redirect_to new_area_path
    end
  end

  def edit
    authorize! :update, Area
  end

  def update
    if area = write("put","api/areas/#{@area.id}",{'area' => area_params})
      area_id = area["response"]["area"]["id"]
      redirect_to areas_path
    else
      redirect_to edit_area_path(@area.id)
    end
  end

  def destroy
    authorize! :delete, Area
    area = write("delete","api/areas/#{@area.id}")
    if area["response"]["message"] == "Deleted Successfully!"
      redirect_to areas_path
    else
      redirect_to areas_path
    end
  end

  private

  def set_area
    @area = Area.find_by(id: params[:id])
  end

  def area_params
    params.require(:area).permit(:name, :city_id, :latitude, :longitude)
  end

end
