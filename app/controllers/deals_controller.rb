class DealsController < ApplicationController


  # load_and_authorize_resource

  before_action :set_business, only: [:new, :edit, :update, :index]
  before_action :set_deal, only: [:show, :edit, :update, :destroy]

  def generate_stamp
    out = write("get","api/deals/#{params[:id]}/generate_stamp",{})
    #punches = write("get","api/deals/#{params[:id]}/stamped_punches",{})
    @out = "Stamp code: " + out["response"]["punched_card_business"]["stamp_code"].to_s 
  end

  def set_frq_deals
    temp = Deal.joins(:business,:branches)
    temp = find_param(temp,"deals",:business_id)
    temp = find_param(temp,"businesses",:business_name,:name)
    temp = find_param(temp,"branches",:branch_name,:name)
    @frq_deals = find_param(temp,"branches",:branch_id,:id)
  end

  def find_param(obj,parent,p,q = nil)
    index = q || p
    col = "#{parent}.#{index.to_s}"
    if params[p].present? && index.to_s.include?("id")
      obj.where("#{col} = #{params[p]}")
    elsif params[p].present? && index.to_s.include?("name")
      obj.where("#{col} like '%#{params[p]}%'")
    else
      obj
    end
  end

  # def frequent_search
  #   if params[:type].to_s == "week_view"
  #     @week_view_frq = true
  #     set_frq_deals
  #     @query_string = params.slice(:business_id,:business_name,:branch_id,:branch_name).to_query
  #   end
  #   week_view
  # end

  def get_ruby_date
    render text: params[:date].to_date.to_s(:db)
  end

  def week_view
    if !params.slice(:business_id,:business_name,:branch_id,:branch_name).blank? && @week_view_frq != true
      @week_view_frq = true
      set_frq_deals
    else
      @week_view = true
    end

    build_q
    get_live_deals
    @week_hsh = WeekView.new(params[:window]).get_week_hsh
    @window = @week_hsh.values.first[0].to_date.to_s
    @first = (@week_hsh.values.first[0].to_date - 10).to_s
    @last = (@week_hsh.values.last[0].to_date + 1).to_s
    @week_view = true
    render "index"
  end

  def index
    authorize! :list, Deal
    @list_view = true
    build_q
    get_live_deals
    @branches_count = Counter.branches(@deals)
  end

  def get_live_deals
    @live_deal_ids = []
    @resp = write("get","api/live/deals",{})
    if !@resp["response"].nil?
      live_deals = @resp["response"]["live_deals"]
      live_deals.each {|key, value| @live_deal_ids.concat(value) if value.length > 0}
    end
  end

  def build_q
    @q = get_model.ransack(params[:q])
    @deals = @q.result.includes(:sub_category,:business,:branches).page(params[:page]).per(params[:limit] || 10)
    3.times {@q.build_condition } if @q.conditions.empty?
  end

  def get_model
    if @week_view_frq
      p "=== im here===:"
      WeekView.deals((params[:date] || Date.today),@frq_deals)
    elsif @week_view
      WeekView.deals(params[:date] || Date.today)
    elsif @list_view
      Deal.order("deals.created_at DESC")
    end
  end

  def new
    authorize! :create, Deal
    @deal = Deal.new
    @deal.buy_start_date = Date.today.to_s(:db)
    @deal.images.build
  end

  def days_map
    {:sun => "sunday", :mon => "monday", :tue => "tuesday", :wed => "wednesday", :thu => "thursday", :fri => "friday", :sat => "saturday"}
  end

  def create
    deal = deal_params
    deal = format_image(deal) if deal["images_attributes"].present?
    deal = change_days(deal)
    deal = change_time(deal)
    deal = format_branches_info(deal)
    deal = process_param(deal)
    Rails.logger.info "================="
    Rails.logger.info deal
    deal = write("post", "api/deals", {'deal' => deal})

    if deal['response']['deal']
      respond_to do |format|
        format.html { redirect_to deals_path }
        format.json { render json: deal }
      end
    else
      respond_to do |format|
        format.html { redirect_to new_deal_path, notice: deal['response'] }
        format.json { render json: deal, status: 400 }
      end
    end

  end

  def edit
    authorize! :update, Deal
    p '@deal.branches_deals'
    p @deal.branches_deals
  end

  def update
    deal = deal_params
    deal = format_image(deal) if deal["images_attributes"].present?
    deal = change_days(deal)
    deal = change_time(deal)
    deal = format_branches_info(deal)
    deal = process_param(deal)

    Rails.logger.info "================="
    Rails.logger.info deal

    deal = write("put", "api/deals/#{@deal.id}", {'deal' => deal})

    Rails.logger.info "================="
    Rails.logger.info deal

    if deal['response']['deal']
      respond_to do |format|
        format.html { redirect_to deals_path }
        format.json { render json: deal }
      end
    else
      respond_to do |format|
        format.html { redirect_to new_deal_path, notice: deal['response'] }
        format.json { render json: deal, status: 400 }
      end
    end

    # if deal =  write("put", "api/deals/@deal.id", {'deals' => deal_params})
    #   redirect_to deal_path(@deal)
    # else
    #   redirect_to deal_path(@business, @deal)
    # end
  end

  def show

  end

  def destroy

    authorize! :delete, Deal
    if @deal.destroy
      redirect_to deals_path
    else
      redirect_to deal_path
    end
  end

  def validate_timings
    response = []
    branch_ids = params[:branch_ids]
    deal_time = params[:deal_time]
    days = params[:days]
    branch_ids.each do |b|
      branch = Branch.find_by(id: b)
      flag = 0
      if !days.nil?
        days.each do |d|
          if flag == 1
            next
          end
          branch_time_slot = BranchTimeSlot.where(branch_id: b).where(day: d)
          if branch_time_slot.size == 0
            response << { branch: b, type: 'd' }
            flag = 1
            next
          elsif branch_time_slot.size == 1
            if deal_time
              if !(branch_time_slot[0].start_time <= time_to_seconds(deal_time[0]) && branch_time_slot[0].end_time <= time_to_seconds(deal_time[1]))
                response << { branch: b, type: 't' }
                flag = 1
                next
              end
            end
          elsif branch_time_slot.size == 2
            if deal_time
              if !(
                  (branch_time_slot[0].start_time <= time_to_seconds(deal_time[0]) && branch_time_slot[0].end_time <= time_to_seconds(deal_time[1])) || 
                  (branch_time_slot[1].start_time <= time_to_seconds(deal_time[0]) && branch_time_slot[1].end_time <= time_to_seconds(deal_time[1])))
                response << { branch: b, type: 't' }
                flag = 1
                next
              end
            end
          end
        end
      end
    end
    render json: response, status: :ok
  end

  private

  def deal_params
    params.require(:deal).permit!#(:name, :branch_id, :original_qty, :description, :price, :deal_price, :start_date, :end_date, :available_days, :highlights, :terms_and_conditions, :redeem_steps, :additional_info, :all_branches, :current_qty, :deal_type, :sub_category_id, :cash_buyable, :app_buyable, :app_post, :bought_count, :redeemed_count, :business_id, :state, :sun, :mon, :tue, :wed, :thu, :fri, :sat,:start_time,:end_time,:images)
  end

  def set_business
    @business = Business.find_by(id: params[:business_id])
  end

  def set_deal
    @deal = Deal.find_by(id: params[:id])
  end

  def change_days(deal)
    deal["available_days"] = []
    deal.keys.each do |k|
      case k
      when "sun"
        deal["available_days"] << "Sunday" if deal[k].to_i == 1
        deal.delete(k)
      when "mon"
        deal["available_days"] << "Monday" if deal[k].to_i == 1
        deal.delete(k)
      when "tue"
        deal["available_days"] << "Tuesday" if deal[k].to_i == 1
        deal.delete(k)
      when "wed"
        deal["available_days"] << "Wednesday" if deal[k].to_i == 1
        deal.delete(k)
      when "thu"
        deal["available_days"] << "Thursday" if deal[k].to_i == 1
        deal.delete(k)
      when "fri"
        deal["available_days"] << "Friday" if deal[k].to_i == 1
        deal.delete(k)
      when "sat"
        deal["available_days"] << "Saturday" if deal[k].to_i == 1
        deal.delete(k)
      else
      end
    end
    deal
  end

  def change_time(p)
    p[:start_time] = time_to_seconds(p[:deal_start_time]) if p[:deal_start_time].present?
    p[:end_time] = time_to_seconds(p[:deal_end_time]) if p[:deal_end_time].present?
    p.delete(:deal_start_time)
    p.delete(:deal_end_time)
    p
  end

  def format_branches_info(deal)
    if deal["original_qty"].to_i > 0 or deal["all_branches"].to_i == 1
      deal["all_branches"] = 1
      deal.delete("branches")
      return deal
    end

    branches_deals = deal["branches"]

    if branches_deals.nil?
      branch_info = []
    elsif deal["limited"].to_i == 1
      branch_info = branches_deals.map {|k,v| v.to_i > 0 ? {"branch_id" => k, "original_qty" => v.to_i} : nil }.compact
    else
      branch_info = branches_deals.map {|k,v|  {"branch_id" => k}  }.compact
    end

    deal["branches_deals"] = branch_info if !branch_info.blank?
    deal.delete("branches")
    deal
  end

  def set_images(deal)
    images = deal["images_attributes"].values
    if !images.blank?
      deal["images"] = images.map { |v| v.except('_destroy')}
    else
      deal["images"] = []
    end
    deal.delete("images_attributes")
    deal
  end

  def process_param(deal)
    [:app_buyable, :cash_buyable, :limited, :all_branches, :app_post, :state].each do |par|
      deal[par] = bool(deal[par])
    end

    if deal.key?('deal_type')
      deal_type = deal['deal_type']
      redeem_steps_text = {
        :regular => '<p style="text-align: center;"><br><strong>HOW DO I GET THIS OFFER?</strong></p><p style="text-align: left;"><strong>Step 1.</strong> Hit the \'Get Now\' button<br><br><strong>Step 2.</strong> Choose your payment method and generate the redemption code.&nbsp;</p><p style="text-align: left;"><strong>Step 3.</strong> Prior to redemption show/quote code to merchant.&nbsp;</p><p>Save big and enjoy the offer!</p><p><img class="fr-dib fr-draggable" src="http://res.cloudinary.com/getpickledeals/image/upload/v1469447044/el9vmiojfgmopi2wwjb8.png" style="width: 300px;" data-public_id="el9vmiojfgmopi2wwjb8" data-version="1469447044" data-signature="d782d5eb203051de32820b9500b76142c9774210" data-width="287" data-height="1" data-format="png" data-resource_type="image" data-created_at="2016-07-25T11:44:04Z" data-tags="" data-bytes="88" data-type="upload" data-etag="0eab1318d0970241cdfda0408992be98" data-url="http://res.cloudinary.com/getpickledeals/image/upload/v1469447044/el9vmiojfgmopi2wwjb8.png" data-secure_url="https://res.cloudinary.com/getpickledeals/image/upload/v1469447044/el9vmiojfgmopi2wwjb8.png" data-original_filename="blob"></p><p><em>Certain offers may require prior booking/reservation. See Description/T&amp;C\'s for further details.</em></p><p>If there are any issues contact Pickle at <a href="mailto:consumer@pickleconnect.com" target="_blank">consumer@pickleconnect.com</a></p>',
        :info => '<p style="text-align: center;"><br><strong>HOW DO I GET THIS OFFER?</strong></p><p>This is an advert regarding an in-store promotion, put up by the merchant as information to the general public. You can avail this offer by simply visiting the mentioned branch running the promotion at the given dates and timings.<br><br><img src="http://res.cloudinary.com/getpickledeals/image/upload/v1469447044/el9vmiojfgmopi2wwjb8.png" data-public_id="el9vmiojfgmopi2wwjb8" data-version="1469447044" data-signature="d782d5eb203051de32820b9500b76142c9774210" data-width="287" data-height="1" data-format="png" data-resource_type="image" data-created_at="2016-07-25T11:44:04Z" data-tags="" data-bytes="88" data-type="upload" data-etag="0eab1318d0970241cdfda0408992be98" data-url="http://res.cloudinary.com/getpickledeals/image/upload/v1469447044/el9vmiojfgmopi2wwjb8.png" data-secure_url="https://res.cloudinary.com/getpickledeals/image/upload/v1469447044/el9vmiojfgmopi2wwjb8.png" data-original_filename="blob" class="fr-dii fr-draggable"></p><p><em>Certain offers may require prior booking/reservation. See Description/T&amp;C\'s for further details.</em></p><p>If there are any issues contact Pickle at <a href="mailto:consumer@pickleconnect.com" target="_blank">consumer@pickleconnect.com</a></p>',
        :punched_cards => '<p style="text-align: center;"><br><strong>HOW DO I GET THIS OFFER?</strong></p><p><strong>Step 1.</strong> Hit the \'Stamp\' button.<br><br><strong>Step 2.</strong> Give code to merchant. &nbsp;</p><p><strong>Step 3.</strong> Receive text message when stamped.</p><p style="text-align: center;"><strong>HOW DO I CLAIM MY REWARD?</strong></p><p><strong>Step 1.</strong> When on the final star, hit the \'Stamp\' button</p><p><strong>Step 2.</strong> Give code to merchant.&nbsp;</p><p><strong>Step 3.</strong> Receive your reward.<br><br><img src="http://res.cloudinary.com/getpickledeals/image/upload/v1469447044/el9vmiojfgmopi2wwjb8.png" data-public_id="el9vmiojfgmopi2wwjb8" data-version="1469447044" data-signature="d782d5eb203051de32820b9500b76142c9774210" data-width="287" data-height="1" data-format="png" data-resource_type="image" data-created_at="2016-07-25T11:44:04Z" data-tags="" data-bytes="88" data-type="upload" data-etag="0eab1318d0970241cdfda0408992be98" data-url="http://res.cloudinary.com/getpickledeals/image/upload/v1469447044/el9vmiojfgmopi2wwjb8.png" data-secure_url="https://res.cloudinary.com/getpickledeals/image/upload/v1469447044/el9vmiojfgmopi2wwjb8.png" data-original_filename="blob" class="fr-dii fr-draggable"></p><p>If there are any issues contact Pickle at <a href="mailto:consumer@pickleconnect.com" target="_blank">consumer@pickleconnect.com</a></p>'
      }
      deal['redeem_steps'] = redeem_steps_text[deal_type.to_sym]

      if ['punched_cards', 'info'].include?(deal_type)
        deal['code_expiry_date'] = deal['end_date'] || deal['buy_end_date']
      end
    end

    # FIX for Froala setting video src to //www.. instead of http://www...
    # Reason: Mobile app cannot understand URLs without http/https protocol
    [:additional_info, :terms_and_conditions].each do |item|
      if !deal[item].nil?
        deal[item] = deal[item].gsub('src="//', 'src="http://')
      end
    end

    if deal[:redeem_type] == 'description'
      deal[:redeem_description] = deal[:additional_info]
    end

    deal.delete("business_name")
    deal
  end


end
