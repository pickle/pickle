class AdminsController < ApplicationController

  #load_and_authorize_resource

  def get_businesses
    admin = Admin.find(params[:admin_id])
    render json: (admin.businesses.select("businesses.id as id,concat(businesses.id,'-',businesses.name) as name").map &:attributes).to_json
  end

  def assign_business
    AdminsBusiness.where(admin_id: params[:admin_id]).delete_all
    params[:business_ids].split(",").each do |b_id|
      AdminsBusiness.create(business_id: b_id, admin_id: params[:admin_id])
    end
  end

  def invite_users
    @admins = Admin.all
    @roles = Array.new
    @roles = Admin::ROLES.each {|role| @roles << role.titleize}
    @invited_users = Admin.where(invited_by_id: current_admin.id)
  end

  def new
  end

  def send_invitation
    if Admin.find_by(email: params[:email]).blank?
      role = params[:role].downcase.gsub(' ','_')
      Admin.invite!({email: params[:email], role: role, reset_password_token: nil}, current_admin)
      redirect_to invite_users_path
    else
      redirect_to invite_users_path
    end
  end

  def reinvite
    if admin = Admin.find_by(id: params[:id])
      admin.invite!(current_admin)
      redirect_to invite_users_path
    end
  end

  def destroy
    @admin = Admin.find(params[:id])
    @admin.destroy
    redirect_to "/invite_users"
  end
end
